#!/bin/bash

ARRAY="10.64.13.200"
VOLCOLL="AH-VolCol"
INITGRP="AH-Backup"
LUNOFFSET=10
SSHKEY="/Users/aherbert/.ssh/nimble"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null admin@${ARRAY} -i ${SSHKEY}"

VOLS=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Associated volumes" | cut -c 21- | tr -d ,)

# Delete any clones linked to existing backup snapshot
for VOL in $VOLS
do
  # Get clone info and test if the volume is a clone
  CLONEINFO=""
  CLONEINFO=$(${SSHCMD} "vol --info ${VOL}-Backup" )
  ISCLONE=""
  ISCLONE=$(echo "${CLONEINFO}" | grep "Clone: " | cut -c 8-)
  DESC=""
  DESC=$(echo "${CLONEINFO}" | grep "Description: " | cut -c 14-)
  if [ "$ISCLONE" == "No" ]
  then
    echo "This volume is not a clone! Something is broken!"
    exit 1
  elif [ "$DESC" == "BackupVolume" ] # Sanity check on description
  then
    # We need to offline and delete this backup clone
    $SSHCMD "vol --offline ${VOL}-Backup --force"
    $SSHCMD "vol --delete ${VOL}-Backup"
  fi
done

# Delete old snapshot now that the clone is gone
${SSHCMD} "snapcoll --delete ${VOLCOLL}-Backup --volcoll ${VOLCOLL}"

#Create new snapshot
${SSHCMD} "volcoll --snap ${VOLCOLL} --snapcoll_name ${VOLCOLL}-Backup"

# Create new clones
LOOP=0
for VOL in $VOLS
do
  (( LOOP += 1 ))
  #Get Volume info
  VOLINFO=""
  VOLINFO=$(${SSHCMD} "vol --info ${VOL}")
  LUN=$LOOP   
  LUN=$(($(echo "${VOLINFO}" | grep "LUN:" | cut -d : -f 2 | tr -d ' ') + LUNOFFSET))
  OLDINITGRP=$(echo "${VOLINFO}" | grep "Initiator Group:" | cut -d : -f 2 | tr -d ' ')

  # Create new clone
  ${SSHCMD} "vol --clone ${VOL} --snapname ${VOLCOLL}-Backup --clonename ${VOL}-Backup --description BackupVolume"
  ${SSHCMD} "vol --removeacl ${VOL}-Backup --initiatorgrp ${OLDINITGRP}"
  ${SSHCMD} "vol --addacl ${VOL}-Backup --initiatorgrp ${INITGRP} --lun ${LUN}"
done
