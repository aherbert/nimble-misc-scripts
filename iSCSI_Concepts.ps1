﻿# Nimble Powershell iSCSI Concepts

# Refresh iSCSI Portals
Function Refresh-Targets {
  $(Get-WmiObject -Namespace root/wmi MSiSCSIInitiator_MethodClass).RefreshTargetList() | Out-Null
}

# Get Available Target IQNs and portal IPs
Function Get-AvailableTargets {
  $TargetClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_TargetClass
  foreach ($Target in $TargetClass) {
    foreach ($Portal in $Target.PortalGroups.Get(0).Portals) {
      New-Object PSObject -Property  @{
        Target = $Target.TargetName
		TargetNice = $Target.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
		Address = $Portal.Address
	    Port = $Portal.Port
	  }
    }
  }
}

# Get list of established sessions and their respective IPs and ports
Function Get-EstablishedSessions {
  $SessionClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_SessionClass
  foreach ($Session in $SessionClass) {
    if ($Session -eq $null) {
      continue;
    }
    $SessionConnectionInformation = $Session.GetPropertyValue("ConnectionInformation").Get(0)
    New-Object PSObject -Property @{
	  Target = $Session.TargetName
	  TargetNice = $Session.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
      Address = $SessionConnectionInformation.TargetAddress
      Port = $SessionConnectionInformation.TargetPort
	  InitiatorAddress = $SessionConnectionInformation.InitiatorAddress
	  Devices = $Session.Devices
    }
  }
}

# Get list of Persistent Logins
Function Get-PersistentLogins {
  $PersistentLoginClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_PersistentLoginClass
  foreach ($PersistentLogin in $PersistentLoginClass) {
    if ($PersistentLogin -eq $null) {
      continue;
    }
	New-Object PSObject -Property @{
	  Target = $PersistentLogin.TargetName
	  TargetNice = $PersistentLogin.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"	  
      Initiator = $PersistentLogin.InitiatorInstance
      InitiatorPort = $PersistentLogin.InitiatorPortNumber
      TargetIP = $PersistentLogin.TargetPortal.Address
	  TargetPort = $PersistentLogin.TargetPortal.Port
	}
  }
}

# Compare two IPs with a subnet mask and return true if they are in the same subnet
Function Compare-Subnet ([string]$ip1, [string]$ip2, [string]$subnet) {
  $octets1 = $ip1 -split '\.'
  $octets2 = $ip2 -split '\.'
  $mask = $subnet -split '\.'
  
  for ($i = 0; $i -lt 4; $i++) {
    $bandip1 += $octets1[$i] -band $mask[$i]
    $bandip2 += $octets2[$i] -band $mask[$i]
  }
  $bandip1 -eq $bandip2
}

# Return all the IPs on the server
Function Get-HostIPs {
  $IPconfigset = Get-WmiObject Win32_NetworkAdapterConfiguration -Namespace "root\CIMv2" | ? {$_.IPEnabled}
  foreach ($ip in $IPconfigset) {
    New-Object PSObject -Property @{
      IPAddress = $ip.IPAddress[0]
	  Subnet = $ip.IPSubnet[0]
    }
  }
}

# Return the iSCSI port and IP mappings
Function Get-iSCSIPorts {
  $PortalInfo = Get-WmiObject -namespace root\wmi MSiSCSI_PortalInfoClass
  $ScriptBlock = {([Net.IPAddress]$_.ipaddr.IPV4Address).IPAddressToString}
  $CustomLabel = @{Label="IPAddress"; expression = $ScriptBlock}
  $PortalInfo.PortalInformation | select Port,$CustomLabel
}

# Add proper MPIO connection by IQN
Function Connect-toIQN ([string]$iqn) {
  foreach ($Login in Get-PersistentLogins | ? {$_.Target -match $iqn}) {
	if ($Login.Target -eq $null) {
      continue
    }
    iscsicli removepersistenttarget $Login.Initiator $Login.Target $Login.InitiatorPort $Login.TargetIP $Login.TargetPort | Out-Null
  }
  foreach ($Target in Get-AvailableTargets | ? {$_.Target -match $iqn}) {
    foreach ($ip in Get-HostIPs) {
      if (Compare-Subnet $Target.Address $ip.IPaddress $ip.Subnet) {
        $iSCSIPort = Get-iSCSIPorts | ? {$_.IPAddress -eq $ip.IPAddress}
	    if (Get-EstablishedSessions | ? {$_.Target -eq $Target.Target -and $_.Address -eq $Target.Address -and $_.InitiatorAddress -eq $ip.IPAddress}) {
	      iscsicli persistentlogintarget $Target.Target T $Target.Address $Target.Port Root\ISCSIPRT\0000_0 $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
        }
        else {
    	  iscsicli logintarget $Target.Target T $Target.Address $Target.Port Root\ISCSIPRT\0000_0 $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
	      iscsicli persistentlogintarget $Target.Target T $Target.Address $Target.Port Root\ISCSIPRT\0000_0 $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
        }
	  }
    }
  }
}

Function Mount-IQN ([string]$iqn, [int]$partition, [string]$path) {
  $Session = Get-EstablishedSessions | ? {$_.Target -match $iqn} | Select Target,TargetNice,Devices -Unique -First 1
  "select disk "+$Session.Devices[0].DeviceNumber+"`n select partition "+$Partition+"`n assign "+$path | diskpart | Out-Null
}

##Initialize Variables
#$NimbleTargets = Get-AvailableTargets | ? {$_.Target -notmatch "com.nimblestorage:control-"} | ? {$_.Target -match "com.nimblestorage"} 
#
## Loop through each Nimble target and host IP and validate connections. If any are missing, execute iscsicli to connect and make persistent 
#foreach ($Target in $NimbleTargets) {
#  Connect-toIQN $Target.Target
#}
#
#
#Exit
#
## Get current set of Nimble sessions
#$NimbleDisks = Get-EstablishedSessions | ? {$_.Target -match "com.nimblestorage"} | Select Target,TargetNice,Devices -Unique
#
## Loop Nimble Disks and make sure they are online and mount if not already mounted
#foreach ($NimbleDisk in $NimbleDisks) {
#  Write-Host "Setting " $NimbleDisk.TargetNice " online"
#  "select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n online disk`n attributes disk clear readonly" | diskpart | Out-Null
#  $Disk = Get-WmiObject -Class Win32_DiskDrive | ? {$_.Caption -match "Nimble Server" -and $_.DeviceID -eq $NimbleDisk.Devices[0].LegacyName}
#  $Partitions = Get-WmiObject -Query "ASSOCIATORS OF {$($Disk.Path.RelativePath)}" | ? {$_.__CLASS -eq "Win32_DiskPartition"}
#  foreach ($Partition in $Partitions) {
#    $LogicalDisk = Get-WmiObject -Query "ASSOCIATORS OF {$($Partition.Path.RelativePath)}" | ? {$_.__CLASS -eq "Win32_LogicalDisk"}
#	if ($LogicalDisk) {
#	  Write-Host "Already Mounted " $NimbleDisk.TargetNice " @ " $LogicalDisk.DeviceID
#	}
#	else {
#	  $Path = "c:\nimble\"+$NimbleDisk.TargetNice+"\"+$Partition.Index
#	  if(Test-Path -Path $Path -PathType Container) {
#	    mountvol $Path /d
#	  }
#	  else {
#	    New-Item $Path -ItemType Directory | Out-Null
#	  }
#      Write-Host "Mounting " $NimbleDisk.TargetNice " @ " $Path
#	  "select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n select partition "+$($Partition.Index + 1)+"`n assign mount="+$Path | diskpart | Out-Null
#    }
#  }
#}
#
#Function Send-NimbleCommand ([string]$host, [string]$key, [string]$command) {
#  'c:\Program Files (x86)\PuTTY\plink.exe' -i $key admin@$host "$command"
#}
#
#Function Get-VolCollSnaps ([string]$volcoll) {
#
#
#
#}
#/ $ snapcoll --list --volcoll <volcoll_name>
#--------------------+---------------------------------------+-------+-----------
#Volume Collection    Snapshot Collection                     Num     Replication
#Name                 Name                                    Snaps   Status
#--------------------+---------------------------------------+-------+-----------
#SQLServer            SQLServer-hourly-2012-05-19::21:00:15.706      2 Complete   
#SQLServer            SQLServer-hourly-2012-05-19::20:00:15.819      2 Complete   
#SQLServer            SQLServer-hourly-2012-05-19::19:00:17.043      2 Complete   
#SQLServer            SQLServer-hourly-2012-05-19::18:00:16.880      2 Complete   
#SQLServer            SQLServer-hourly-2012-05-19::17:00:16.198      2 Complete   
#
#
#/ $ snapcoll --info <snapcoll_name>           
#Name: SQLServer-hourly-2012-05-18::14:00:16.088
#Description: VSS snapshot
#Volume collection name: SQLServer
#Origination group name: Nimble01
#Is replica: No
#Replication status: Complete
#Replication started: May 20 2012 02:40:57
#Replication completed: May 20 2012 02:40:58
#Replication bytes transferred: 25724
#Created: May 18 2012 19:00:16
#Snapshots: 
#	Volume: SQL-Data
#		Snapshot: SQLServer-hourly-2012-05-18::14:00:16.088
#	Volume: SQL-Logs
#		Snapshot: SQLServer-hourly-2012-05-18::14:00:16.088
#
#vol   --clone volname   [--snapname snapname]   [--clonename clonename]
#       [--description text]   [--readonly { yes | no }]    [--reserve percent]
#       [--quota percent]    [--warn_level percent]    [--snap_reserve percent]
#       [--snap_quota percent]  [--snap_warn_level percent]   [--start_offline]
#       [--apply_acl_to {volume|snapshot|both}]  [--chapuser username]  [--ini-
#       tiatorgrp group_name] [--multi_initiator { yes | no }]
#
#/ $ vol --clone SQL-Data --snapname SQLServer-hourly-2012-05-18::14:00:16.088 --clonename SQL-Data-TestClone --description "Automated Clone" --readonly no --apply_acl_to both --initiatorgrp esx-node3
#
#/ $ vol --info SQL-Data-TestClone
#Name: SQL-Data-TestClone
#Serial number: 709e4db1c9fd52476c9ce900a9bf50ae
#iSCSI target: iqn.2007-11.com.nimblestorage:sql-data-testclone-v599eafd8f7ce1071.0000003d.ae50bfa9
#Description: Automated Clone
#Owned by: Nimble01
#Size (MB): 102400
#Performance policy: SQL Server
#Block size (bytes): 8192
#Reserve: 0.00%
#Warn level: 80.00%
#Quota: 100.00%
#Snapshot reserve: 0.00%
#Snapshot warn level: N/A
#Snapshot quota: unlimited
#Volume usage (MB): unknown
#Volume compression: unknown
#Volume space saved (MB): unknown
#Snapshot usage (MB): unknown
#Snapshot compression: unknown
#Snapshot space reduction: unknown
#Snapshot space saved (MB): unknown
#Read only: No
#Multi-initiator: Yes
#Online: Yes
#Offline reason: N/A
#Clone: Yes
#Parent volume: SQL-Data
#Base snapshot: SQLServer-hourly-2012-05-18::14:00:16.088
#Volume collection: none
#Number of connections: unknown
#Created: May 20 2012 03:00:19
#Last configuration change: May 20 2012 03:00:19
#Access Control List: 
#	Apply to: volume & snapshot
#	Initiator Group: esx-node3
#	CHAP user: *
#Connected Initiators: 
#