﻿# Nimble Powershell iSCSI Concepts

# Refresh iSCSI Portals
Function Refresh-Targets {
  $(Get-WmiObject -Namespace root/wmi MSiSCSIInitiator_MethodClass).RefreshTargetList() | Out-Null
}

# Get Available Target IQNs and portal IPs
Function Get-AvailableTargets {
  $TargetClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_TargetClass
  foreach ($Target in $TargetClass) {
    foreach ($Portal in $Target.PortalGroups.Get(0).Portals) {
      New-Object PSObject -Property  @{
        Target = $Target.TargetName
		TargetNice = $Target.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
		Address = $Portal.Address
	    Port = $Portal.Port
	  }
    }
  }
}

# Get list of established sessions and their respective IPs and ports
Function Get-EstablishedSessions {
  $SessionClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_SessionClass
  foreach ($Session in $SessionClass) {
    if ($Session -eq $null) {
      continue;
    }
    $SessionConnectionInformation = $Session.GetPropertyValue("ConnectionInformation").Get(0)
    New-Object PSObject -Property @{
	  Target = $Session.TargetName
	  TargetNice = $Session.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
      Address = $SessionConnectionInformation.TargetAddress
      Port = $SessionConnectionInformation.TargetPort
	  InitiatorAddress = $SessionConnectionInformation.InitiatorAddress
	  Devices = $Session.Devices
    }
  }
}

# Get list of Persistent Logins
Function Get-PersistentLogins {
  $PersistentLoginClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_PersistentLoginClass
  foreach ($PersistentLogin in $PersistentLoginClass) {
    if ($PersistentLogin -eq $null) {
      continue;
    }
	New-Object PSObject -Property @{
	  Target = $PersistentLogin.TargetName
	  TargetNice = $PersistentLogin.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"	  
      Initiator = $PersistentLogin.InitiatorInstance
      InitiatorPort = $PersistentLogin.InitiatorPortNumber
      TargetIP = $PersistentLogin.TargetPortal.Address
	  TargetPort = $PersistentLogin.TargetPortal.Port
	}
  }
}

# Compare two IPs with a subnet mask and return true if they are in the same subnet
Function Compare-Subnet ([string]$ip1, [string]$ip2, [string]$subnet) {
  $octets1 = $ip1 -split '\.'
  $octets2 = $ip2 -split '\.'
  $mask = $subnet -split '\.'
  
  for ($i = 0; $i -lt 4; $i++) {
    $bandip1 += $octets1[$i] -band $mask[$i]
    $bandip2 += $octets2[$i] -band $mask[$i]
  }
  $bandip1 -eq $bandip2
}

# Return all the IPs on the server
Function Get-HostIPs {
  $IPconfigset = Get-WmiObject Win32_NetworkAdapterConfiguration -Namespace "root\CIMv2" | ? {$_.IPEnabled}
  foreach ($ip in $IPconfigset) {
    New-Object PSObject -Property @{
      IPAddress = $ip.IPAddress[0]
	  Subnet = $ip.IPSubnet[0]
    }
  }
}

# Return the iSCSI port and IP mappings
Function Get-iSCSIPorts {
  $InitiatorInfo = Get-WmiObject -namespace root\wmi MSiSCSI_PortalInfoClass
  foreach ($Initiator in $InitiatorInfo) {  
    foreach ($Port in $Initiator.PortalInformation) {
	New-Object -TypeName PSObject -Property @{
	   "InitiatorName" = $Initiator.InstanceName;
	   "Port" = $Port.Port;
	   "IPAddress" = ([Net.IPAddress]$Port.IpAddr.IPV4Address).IPAddressToString
	  }
	}
  }
}

# Add proper MPIO connection by IQN
Function Connect-toIQN ([string]$iqn) {
  foreach ($Login in Get-PersistentLogins | ? {$_.Target -match $iqn}) {
	if ($Login.Target -eq $null) {
      continue
    }
    iscsicli removepersistenttarget $Login.Initiator $Login.Target $Login.InitiatorPort $Login.TargetIP $Login.TargetPort | Out-Null
  }
  foreach ($Target in Get-AvailableTargets | ? {$_.Target -match $iqn}) {
    foreach ($ip in Get-HostIPs) {
      if (Compare-Subnet $Target.Address $ip.IPaddress $ip.Subnet) {
        $iSCSIPort = Get-iSCSIPorts | ? {$_.IPAddress -eq $ip.IPAddress}
	    if (Get-EstablishedSessions | ? {$_.Target -eq $Target.Target -and $_.Address -eq $Target.Address -and $_.InitiatorAddress -eq $ip.IPAddress}) {
	      iscsicli persistentlogintarget $Target.Target T $Target.Address $Target.Port $iSCSIPort.InitiatorName $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
        }
        else {
    	  iscsicli logintarget $Target.Target T $Target.Address $Target.Port $iSCSIPort.InitiatorName $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
	      iscsicli persistentlogintarget $Target.Target T $Target.Address $Target.Port $iSCSIPort.InitiatorName $iSCSIPort.port * 0x2 * * * * * * * * * 0 | Out-Null
        }
	  }
    }
  }
}

Function Mount-IQN ([string]$iqn, [int]$partition, [string]$path) {
  $Session = Get-EstablishedSessions | ? {$_.Target -match $iqn} | Select Target,TargetNice,Devices -Unique -First 1
  "select disk "+$Session.Devices[0].DeviceNumber+"`n select partition "+$partition+"`n attributes volume clear readonly`n attributes volume clear hidden`n assign "+$path | diskpart | Out-Null
}
# SIG # Begin signature block
# MIIQfQYJKoZIhvcNAQcCoIIQbjCCEGoCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUjMTKL+yt6kQK2g06Wp5YooRO
# SPGggg2yMIIGcDCCBFigAwIBAgIBJDANBgkqhkiG9w0BAQUFADB9MQswCQYDVQQG
# EwJJTDEWMBQGA1UEChMNU3RhcnRDb20gTHRkLjErMCkGA1UECxMiU2VjdXJlIERp
# Z2l0YWwgQ2VydGlmaWNhdGUgU2lnbmluZzEpMCcGA1UEAxMgU3RhcnRDb20gQ2Vy
# dGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMDcxMDI0MjIwMTQ2WhcNMTcxMDI0MjIw
# MTQ2WjCBjDELMAkGA1UEBhMCSUwxFjAUBgNVBAoTDVN0YXJ0Q29tIEx0ZC4xKzAp
# BgNVBAsTIlNlY3VyZSBEaWdpdGFsIENlcnRpZmljYXRlIFNpZ25pbmcxODA2BgNV
# BAMTL1N0YXJ0Q29tIENsYXNzIDIgUHJpbWFyeSBJbnRlcm1lZGlhdGUgT2JqZWN0
# IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyiOLIjUemqAbPJ1J
# 0D8MlzgWKbr4fYlbRVjvhHDtfhFN6RQxq0PjTQxRgWzwFQNKJCdU5ftKoM5N4YSj
# Id6ZNavcSa6/McVnhDAQm+8H3HWoD030NVOxbjgD/Ih3HaV3/z9159nnvyxQEckR
# ZfpJB2Kfk6aHqW3JnSvRe+XVZSufDVCe/vtxGSEwKCaNrsLc9pboUoYIC3oyzWoU
# TZ65+c0H4paR8c8eK/mC914mBo6N0dQ512/bkSdaeY9YaQpGtW/h/W/FkbQRT3sC
# pttLVlIjnkuY4r9+zvqhToPjxcfDYEf+XD8VGkAqle8Aa8hQ+M1qGdQjAye8OzbV
# uUOw7wIDAQABo4IB6TCCAeUwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC
# AQYwHQYDVR0OBBYEFNBOD0CZbLhLGW87KLjg44gHNKq3MB8GA1UdIwQYMBaAFE4L
# 7xqkQFulF2mHMMo0aEPQQa7yMD0GCCsGAQUFBwEBBDEwLzAtBggrBgEFBQcwAoYh
# aHR0cDovL3d3dy5zdGFydHNzbC5jb20vc2ZzY2EuY3J0MFsGA1UdHwRUMFIwJ6Al
# oCOGIWh0dHA6Ly93d3cuc3RhcnRzc2wuY29tL3Nmc2NhLmNybDAnoCWgI4YhaHR0
# cDovL2NybC5zdGFydHNzbC5jb20vc2ZzY2EuY3JsMIGABgNVHSAEeTB3MHUGCysG
# AQQBgbU3AQIBMGYwLgYIKwYBBQUHAgEWImh0dHA6Ly93d3cuc3RhcnRzc2wuY29t
# L3BvbGljeS5wZGYwNAYIKwYBBQUHAgEWKGh0dHA6Ly93d3cuc3RhcnRzc2wuY29t
# L2ludGVybWVkaWF0ZS5wZGYwEQYJYIZIAYb4QgEBBAQDAgABMFAGCWCGSAGG+EIB
# DQRDFkFTdGFydENvbSBDbGFzcyAyIFByaW1hcnkgSW50ZXJtZWRpYXRlIE9iamVj
# dCBTaWduaW5nIENlcnRpZmljYXRlczANBgkqhkiG9w0BAQUFAAOCAgEAcnMLA3Va
# N4OIE9l4QT5OEtZy5PByBit3oHiqQpgVEQo7DHRsjXD5H/IyTivpMikaaeRxIv95
# baRd4hoUcMwDj4JIjC3WA9FoNFV31SMljEZa66G8RQECdMSSufgfDYu1XQ+cUKxh
# D3EtLGGcFGjjML7EQv2Iol741rEsycXwIXcryxeiMbU2TPi7X3elbwQMc4JFlJ4B
# y9FhBzuZB1DV2sN2irGVbC3G/1+S2doPDjL1CaElwRa/T0qkq2vvPxUgryAoCppU
# FKViw5yoGYC+z1GaesWWiP1eFKAL0wI7IgSvLzU3y1Vp7vsYaxOVBqZtebFTWRHt
# XjCsFrrQBngt0d33QbQRI5mwgzEp7XJ9xu5d6RVWM4TPRUsd+DDZpBHm9mszvi9g
# VFb2ZG7qRRXCSqys4+u/NLBPbXi/m/lU00cODQTlC/euwjk9HQtRrXQ/zqsBJS6U
# J+eLGw1qOfj+HVBl/ZQpfoLk7IoWlRQvRL1s7oirEaqPZUIWY/grXq9r6jDKAp3L
# ZdKQpPOnnogtqlU4f7/kLjEJhrrc98mrOWmVMK/BuFRAfQ5oDUMnVmCzAzLMjKfG
# cVW/iMew41yfhgKbwpfzm3LBr1Zv+pEBgcgW6onRLSAn3XHM0eNtz+AkxH6rRf6B
# 2mYhLEEGLapH8R1AMAo4BbVFOZR5kXcMCwowggc6MIIGIqADAgECAgIGzjANBgkq
# hkiG9w0BAQUFADCBjDELMAkGA1UEBhMCSUwxFjAUBgNVBAoTDVN0YXJ0Q29tIEx0
# ZC4xKzApBgNVBAsTIlNlY3VyZSBEaWdpdGFsIENlcnRpZmljYXRlIFNpZ25pbmcx
# ODA2BgNVBAMTL1N0YXJ0Q29tIENsYXNzIDIgUHJpbWFyeSBJbnRlcm1lZGlhdGUg
# T2JqZWN0IENBMB4XDTEyMDcyOTIyNTAxMFoXDTE0MDczMTA2NTk1NlowgZExGTAX
# BgNVBA0TEHJyaVdETDZkejRzdTFuU1gxCzAJBgNVBAYTAlVTMRIwEAYDVQQIEwlU
# ZW5uZXNzZWUxEzARBgNVBAcTCkdyZWVuYnJpZXIxFTATBgNVBAMTDEFkYW0gSGVy
# YmVydDEnMCUGCSqGSIb3DQEJARYYYWRhbS5oLmhlcmJlcnRAZ21haWwuY29tMIIB
# IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4QdlmfR24YXQg4L9cuX/6owc
# LTZSv/YXQjCAkZXJxy1n77M8Q5wg/ByylDgLHwpXmdBDI/cYxIc5wk/Rr41Nav6m
# p99jGazEaYGiWisxv+oJAMc+XIwdCHht6cyDASoapiDIuyPkJvnKs8jL96ozXCyj
# JiGKZU0uQfFraD0QSmv2wNztg0VVGFxgYuJ/VTcQhDYRaJIo6NtBxbFSLlnQWAFb
# FIs1GUrK7Zd9yr/lWl4qEI8Cue1rlHcz+xG8Q0vmMIDjt2eSkjxJZRsJC+KeRMct
# YTfnlPB6QMB2pYOBcBagbgUtc9QkmUR5s0FVwcsn9CuYx43BckItMtvQw/ZOhwID
# AQABo4IDnTCCA5kwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCB4AwLgYDVR0lAQH/
# BCQwIgYIKwYBBQUHAwMGCisGAQQBgjcCARUGCisGAQQBgjcKAw0wHQYDVR0OBBYE
# FCuJvocvoz2bvxcCC0jvN0DE4zz+MB8GA1UdIwQYMBaAFNBOD0CZbLhLGW87KLjg
# 44gHNKq3MIICIQYDVR0gBIICGDCCAhQwggIQBgsrBgEEAYG1NwECAjCCAf8wLgYI
# KwYBBQUHAgEWImh0dHA6Ly93d3cuc3RhcnRzc2wuY29tL3BvbGljeS5wZGYwNAYI
# KwYBBQUHAgEWKGh0dHA6Ly93d3cuc3RhcnRzc2wuY29tL2ludGVybWVkaWF0ZS5w
# ZGYwgfcGCCsGAQUFBwICMIHqMCcWIFN0YXJ0Q29tIENlcnRpZmljYXRpb24gQXV0
# aG9yaXR5MAMCAQEagb5UaGlzIGNlcnRpZmljYXRlIHdhcyBpc3N1ZWQgYWNjb3Jk
# aW5nIHRvIHRoZSBDbGFzcyAyIFZhbGlkYXRpb24gcmVxdWlyZW1lbnRzIG9mIHRo
# ZSBTdGFydENvbSBDQSBwb2xpY3ksIHJlbGlhbmNlIG9ubHkgZm9yIHRoZSBpbnRl
# bmRlZCBwdXJwb3NlIGluIGNvbXBsaWFuY2Ugb2YgdGhlIHJlbHlpbmcgcGFydHkg
# b2JsaWdhdGlvbnMuMIGcBggrBgEFBQcCAjCBjzAnFiBTdGFydENvbSBDZXJ0aWZp
# Y2F0aW9uIEF1dGhvcml0eTADAgECGmRMaWFiaWxpdHkgYW5kIHdhcnJhbnRpZXMg
# YXJlIGxpbWl0ZWQhIFNlZSBzZWN0aW9uICJMZWdhbCBhbmQgTGltaXRhdGlvbnMi
# IG9mIHRoZSBTdGFydENvbSBDQSBwb2xpY3kuMDYGA1UdHwQvMC0wK6ApoCeGJWh0
# dHA6Ly9jcmwuc3RhcnRzc2wuY29tL2NydGMyLWNybC5jcmwwgYkGCCsGAQUFBwEB
# BH0wezA3BggrBgEFBQcwAYYraHR0cDovL29jc3Auc3RhcnRzc2wuY29tL3N1Yi9j
# bGFzczIvY29kZS9jYTBABggrBgEFBQcwAoY0aHR0cDovL2FpYS5zdGFydHNzbC5j
# b20vY2VydHMvc3ViLmNsYXNzMi5jb2RlLmNhLmNydDAjBgNVHRIEHDAahhhodHRw
# Oi8vd3d3LnN0YXJ0c3NsLmNvbS8wDQYJKoZIhvcNAQEFBQADggEBAFDIXR2gROrL
# d6b5n8aGp8yf6Yv8gV/sn9ziTbccJwJ2GisDDgbWQjSsn0GnlEONO/3d1NPrkzZp
# XjHpA50mYZluu+SWNeUiMmlVPOLM7OzovndR/mzGnT5NzUx6kbwf7cuOqlVKgqbe
# 4B8qn0mMVK5A+pc3hJMSq9O9s2LzeTEA4LFcU1OkBvxRTJ5BEj/CHV8ydsMz1A6N
# vP1MSKix5lcZ8cxwXtJeXQnlZd9NXiMq4AD/SwUPS19PT8Z21lDkXN9ZneClML0B
# hYMQyxQjAEUx5yd2KSqc4Erfu33yHdh/Hxr7WV6+9Emu3VN5nLY4ef66dpCP/Jrn
# PCdlS/fzkX4xggI1MIICMQIBATCBkzCBjDELMAkGA1UEBhMCSUwxFjAUBgNVBAoT
# DVN0YXJ0Q29tIEx0ZC4xKzApBgNVBAsTIlNlY3VyZSBEaWdpdGFsIENlcnRpZmlj
# YXRlIFNpZ25pbmcxODA2BgNVBAMTL1N0YXJ0Q29tIENsYXNzIDIgUHJpbWFyeSBJ
# bnRlcm1lZGlhdGUgT2JqZWN0IENBAgIGzjAJBgUrDgMCGgUAoHgwGAYKKwYBBAGC
# NwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgor
# BgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUnR/9sNQf
# vrTsxm+IUy53LC9VY20wDQYJKoZIhvcNAQEBBQAEggEALJX5VG41DZerxeGbxRE2
# CxQwVu6dI8JC0fb3DKs+6E8jYbcuKfHhkkjZTmDMdaT53DzVVc8Tbm0bOYW915kk
# /apqLbmvm9CF/uhhRd7Qx5Rk4pekQQvT/R+dRM6Ghw6aGubkjt27a5HN3H6Ir+fo
# ++hSe7dmjih0wEB1sMTzQl7X+zdKuftOaCAeXZfBBPVUipWNlG3O6tt+7gqlU17y
# fb43CtktKC3Vcn2jejjOETHNdavCiqQTOxxVuqJcOV5UHIw73TV+Dgm38y6y9aEn
# nkbRl8M3i6SGtZjNvrzE1UT4/QTWCZvTQtv9KFiyaEu+xTZG790zZX2iNutLDJ3+
# qA==
# SIG # End signature block
