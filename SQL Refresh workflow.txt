1. Select SQL instance to refresh
2. Enumerate and select snapshot for volume collection
 - snapcoll --volcoll SQL-Servers --list
3. Shutdown selected SQL instance
4. Unmount and delete old clone
 - mountvol <drive> /d
5. Remove volume from iSCSI favorites
 - iscsicli 
6. Create clone on volumes from selected snapshot
7. Attach with iSCSI initiator and mount to proper mount point for the instance
8. Restart SQL server instance