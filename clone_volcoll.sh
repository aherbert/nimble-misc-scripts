#!/bin/bash

ARRAY="rtp-afa31.rtplab.nimblestorage.com"
VOLCOLL="CentOS"
SUFFIX="Clone"
INITGRP="CentOS"
SSHKEY="/root/.ssh/nimble"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null admin@${ARRAY} -i ${SSHKEY}"

VOLS=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Associated volumes" | cut -c 21- | tr -d ,)

# Delete any clones linked to existing backup snapshot
for VOL in $VOLS
do
  # Unmount old clone
  umount /mnt/${VOL}-${SUFFIX} 2> /dev/null
  iscsiadm -m node -u -T $(iscsiadm -m node | egrep -i ":${VOL}-${SUFFIX}-[^-]*$" | cut -d " " -f 2 | uniq) > /dev/null
  # Get clone info and test if the volume is a clone
  CLONEINFO=$(${SSHCMD} "vol --info ${VOL}-${SUFFIX}" )
  ISCLONE=$(echo "${CLONEINFO}" | grep "Clone: " | cut -c 8-)
  DESC=$(echo "${CLONEINFO}" | grep "Description: " | cut -c 14-)
  if [ "$ISCLONE" == "No" ]
  then
    echo "This volume is not a clone! Something is broken!"
    exit 1
  elif [ "$DESC" == "CloneVolume" ] # Sanity check on description
  then
    # We need to offline and delete this backup clone
    $SSHCMD "vol --offline ${VOL}-${SUFFIX} --force" 2>/dev/null
    $SSHCMD "vol --delete ${VOL}-${SUFFIX}"
  fi
done

# Delete old snapshot now that the clone is gone
${SSHCMD} "snapcoll --delete ${VOLCOLL}-${SUFFIX} --volcoll ${VOLCOLL}"

#Create new snapshot
${SSHCMD} "volcoll --snap ${VOLCOLL} --snapcoll_name ${VOLCOLL}-${SUFFIX}"

# Create new clones
for VOL in $VOLS
do
  # Create new clone
  ${SSHCMD} "vol --clone ${VOL} --snapname ${VOLCOLL}-${SUFFIX} --clonename ${VOL}-${SUFFIX} --initiatorgrp ${INITGRP} --description CloneVolume"
done

iscsiadm -m discovery | cut -d" " -f 1 | xargs -L1 iscsiadm -m discovery -t sendtargets -p > /dev/null
for VOL in $VOLS
do
  #Scan for new target and connect
  iscsiadm -m node -l -T $(iscsiadm -m node | egrep -i ":${VOL}-${SUFFIX}-[^-]*$" | cut -d" " -f 2 | uniq) > /dev/null
done

sleep 10
multipath -r -v 0

MAPPER=""
for x in `multipath -ll | grep Nimble | cut -d " " -f 1`
do
  MAPPER+="/dev/mapper/$x "
  MAPPER+=`sg_inq -p0xd4 -r /dev/mapper/$x | tr -d '\200-\377'`
  MAPPER+="\n"
done

for VOL in $VOLS
do
  echo -e $MAPPER | grep ${VOL}-${SUFFIX}
  mkdir -p /mnt/${VOL}-${SUFFIX}
  DEVICE=`echo -e $MAPPER | grep ${VOL}-${SUFFIX} | cut -d " " -f 1`
  mount $DEVICE /mnt/${VOL}-${SUFFIX}
done