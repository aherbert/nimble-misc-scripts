#Require -Version 3.0

[CmdletBinding()]
param (
  # VCenter Host
  [Parameter(Mandatory = $true)]
  [String]
  $VCenterName,
  # VCenter Credentials
  [Parameter(Mandatory = $false)]
  [pscredential]
  $VCenterCredential,
  # Target VM Name
  [Parameter(Mandatory = $true)]
  [string]
  $VMName,
  # Nimble Group
  [Parameter(Mandatory = $true)]
  [string]
  $NimbleGroup,
  # Nimble Credential
  [Parameter(Mandatory = $false)]
  [pscredential]
  $NimbleCredential,
  # Nimble Volumes 
  [Parameter(Mandatory = $true)]
  [string[]]
  $NimbleVolume,
  # Action
  [Parameter(Mandatory = $false)]
  [ValidateSet("Refresh", "Create", "Remove")]
  [string]
  $Action = "Refresh",
  # Snapshot selection (0 = New, 1 = latest, 2 = second snapshot, etc.)
  # If snapshot number is too large it will use oldest snapshot
  [Parameter(Mandatory = $false)]
  [int]
  $Snapshot = 0,
  # Initiator group of VMware Cluster
  [Parameter(Mandatory = $false)]
  [string]
  $InitiatorGroup
)

$ErrorActionPreference = "Stop"

function Main {
  if (!(Get-Module -Name VMware.VimAutomation.Core) -and (Get-Module -ListAvailable -Name VMware.VimAutomation.Core)) {
    Import-Module -Name VMware.VimAutomation.Core
  }
  elseif (!(Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -and !(Get-Module -Name VMware.VimAutomation.Core) -and ($Loaded -ne $True)) {
    Add-PSSnapin -PassThru VMware.VimAutomation.Core
  }

  $dateStamp = "{0:X}" -f [int](New-TimeSpan -Start (Get-Date "1/1/1970") -End (Get-Date)).TotalSeconds
  
  Connect-VCenter | Out-Null
  Connect-NSGroup | Out-Null

  $vm = Get-VM -Name $VMName

  foreach ($parentVolumeName in $NimbleVolume) {
    $NSVolumeObject = Get-NSVolume -RequestArgument @{ 
      "metadata.script"   = "NimbleRDMClone"
      "metadata.targetvm" = $VMName
      "parent_vol_name"   = $parentVolumeName
      "online"            = "true"
    }
    if ($Action -in @("Refresh", "Remove") -and $NSVolumeObject) {
      Get-HardDisk -vm $vm -DiskType RawPhysical | Where-Object { $_.ScsiCanonicalName -eq "eui." + $NSVolumeObject.serial_number } | Remove-HardDisk -Confirm:$false -DeletePermanently
      $NSVolumeObject | Set-NSVolume -Online:$false -Force:$true | Out-Null
      $NSVolumeObject | Set-NSVolume -RequestArgument @{ "name" = [string]$($NSVolumeObject.name + "-deleted") } -Metadata @{ "delete" = "true" } | Out-Null
    }
    elseif ($NSVolumeObject -ne $null) {
      Write-Error "Volume clone for $parentVolumeName to $VMName already exists!"
    }

    # Create new clone
    if ($Action -in @("Refresh", "Create")) {
      $metadata = @{ "script" = "NimbleRDMClone"; "targetvm" = $VMName }
      $parentVolumeObject = Get-NSVolume -RequestArgument @{ name = $parentVolumeName }
      if ($Snapshot -eq 0) {
        $snapshotObject = New-NSSnapshot -Volume $parentVolumeObject -SnapshotName "NimbleRDMClone-$VMName-$datestamp" -Description "Snapshot for NimbleRDMClone" -Metadata $metadata    
      }
      else {
        $snapshotObjectList = Get-NSSnapshot -RequestArgument @{ "vol_id" = $parentVolumeObject.id } | Sort-Object -Descending creation_time
        if ($snapshotObjectList.count -eq 0) {
          $snapshotObject = New-NSSnapshot -Volume $parentVolumeObject -SnapshotName "NimbleRDMClone for $VMName" -Description "Snapshot for NimbleRDMClone" -Metadata $metadata
        }
        elseif ($snapshotObjectList.count -lt ($Snapshot - 1)) {
          $snapshotObject = $snapshotObjectList[-1]
        }
        else {
          $snapshotObject = $snapshotObjectList[($Snapshot - 1)]
        }
      }
      $cloneVolumeObject = New-NSSnapshotClone -SnapshotObject $snapshotObject -CloneName ("$parentVolumeName-$VMName-$datestamp").ToLower() -Description "NimbleRDMClone from $parentVolumeName to $VMName" -Metadata $metadata
      if ($InitiatorGroup) {
        @($cloneVolumeObject) | Set-NSVolumeInitiatorGroup -InitiatorGroupName $InitiatorGroup
      }
      $cloneVolumeObject | Set-NSVolume -Online:$true | Out-Null
      $vm.VMHost | Get-VMHostStorage -RescanAllHba | Out-Null
      $scsiLun = Get-ScsiLun -VmHost $vm.VMHost | Where-Object { $_.CanonicalName -eq "eui." + $cloneVolumeObject.serial_number }
      New-HardDisk -VM $vm -DiskType RawPhysical -DeviceName $scsiLun.ConsoleDeviceName -WarningAction SilentlyContinue | Out-Null
    }
  }

  $vm.VMHost.Parent | Get-VMHost | Get-VMHostStorage -RescanAllHba | Out-Null
  Disconnect-VIServer -Force -Confirm:$false

  # Cleanup Deleted Volumes
  $deletedVolumeObjectList = Get-NSVolume -RequestArgument @{"metadata.script" = "NimbleRDMClone"; "metadata.delete" = "true"; "online" = "false"}
  if ($deletedVolumeObjectList) {
    $deletedVolumeObjectList | Remove-NSVolume
    $deletedVolumeObjectList | Select-Object -Expand base_snap_id | Get-NSSnapshot | Where-Object { $_.metadata.value -eq "NimbleRDMClone" } | Remove-NSSnapshot
  }  
}
Function Connect-VCenter {
  param(
    # VCenter Server Name
    [Parameter(Mandatory = $false)]
    [string]$VCenterName = $Script:VCenterName,
    # VCenter Credential
    [Parameter(Mandatory = $false)]
    [pscredential]
    $VCenterCredential = $Script:VCenterCredential
  )


  <# Get VCenter credential from registry #>
  if (!(Test-Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName")) { 
    $storedCredential = New-Item -Path "HKLM:\Software\NimbleStorage\Credentials" -Name "VCenter-$VCenterName" -Force -ErrorAction Continue
  }
  else {
    $storedCredential = Get-Item -Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName"
  }
  <##>

  <# Create credential object #>
  if (!$ResetPassword) {
    try {
      $vcenterUsername = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName" -Name "Username").Username
      $passphrase = Get-StringHash -String $($vcenterUsername + $VCenterName)
      $vcenterEncryptedPassword = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName" -Name "Password").Password
      $vcenterPassword = Decrypt-String -Encrypted $vcenterEncryptedPassword -Passphrase $passphrase -salt $vcenterUsername -init $VCenterName | ConvertTo-SecureString -AsPlainText -Force
      $VCenterCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $vcenterUsername, $vcenterPassword
    }
    catch { }
  }
  if (!$VCenterCredential -or $ResetPassword) {
    $VCenterCredential = Get-Credential -Message "Enter username and password for $VCenterName"
    try {
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName" -Name "Username" -Value $VCenterCredential.UserName
      $passphrase = Get-StringHash -String $($VCenterCredential.UserName + $VCenterName)
      $vcenterEncryptedPassword = Encrypt-String -String ($VCenterCredential.GetNetworkCredential().Password) -Passphrase $passphrase -salt $VCenterCredential.UserName -init $VCenterName
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\VCenter-$VCenterName" -Name "Password" -Value $vcenterEncryptedPassword
    }
    catch { }
  }
  <##>

  try {
    $viserver = Connect-VIServer -Server $VCenterName -Credential $VCenterCredential -WarningAction SilentlyContinue
  }
  catch {
    $Global:viserver = $null
  }

  $Global:viserver = $viserver
  return $viserver
}

Function Connect-NSGroup {
  param(
    # Nimble Group Name
    [Parameter(Mandatory = $false)]
    [string]$NimbleGroup = $Script:NimbleGroup,
    # Nimble Credential
    [Parameter(Mandatory = $false)]
    [pscredential]
    $NimbleCredential = $Script:NimbleCredential
  )

  if ($Global:session_token) {
    try {
      $result = Send-NSRequest -RequestMethod get -RequestTarget "tokens/detail" -RequestArguments @{ "session_token" = $Global:session_token }
      return $result.data
    }
    catch {
      $Global:session_token = $null
    }
  }

  <# Get Nimble credential from registry #>
  if (!(Test-Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup")) { 
    $storedCredential = New-Item -Path "HKLM:\Software\NimbleStorage\Credentials" -Name $NimbleGroup -Force -ErrorAction Continue
  }
  else {
    $storedCredential = Get-Item -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup"
  }
  <##>

  <# Create credential object #>
  if (!$ResetPassword) {
    try {
      $nimbleUsername = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username").Username
      $passphrase = Get-StringHash -String $($nimbleUsername + $NimbleGroup)
      $nimbleEncryptedPassword = (Get-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password").Password
      $nimblePassword = Decrypt-String -Encrypted $nimbleEncryptedPassword -Passphrase $passphrase -salt $nimbleUsername -init $NimbleGroup | ConvertTo-SecureString -AsPlainText -Force
      $nimbleCredential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nimbleUsername, $nimblePassword
    }
    catch { }
  }
  if (!$nimbleCredential -or $ResetPassword) {
    $nimbleCredential = Get-Credential -Message "Enter username and password for $NimbleGroup"
    try {
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Username" -Value $nimbleCredential.UserName
      $passphrase = Get-StringHash -String $($nimbleCredential.UserName + $NimbleGroup)
      $nimbleEncryptedPassword = Encrypt-String -String ($nimbleCredential.GetNetworkCredential().Password) -Passphrase $passphrase -salt $nimbleCredential.UserName -init $NimbleGroup
      Set-ItemProperty -Path "HKLM:\Software\NimbleStorage\Credentials\$NimbleGroup" -Name "Password" -Value $nimbleEncryptedPassword
    }
    catch { }
  }
  <##>

  $scriptName = & { $MyInvocation.ScriptName } | Split-Path -Leaf
  $networkCredential = $nimbleCredential.GetNetworkCredential()
  $result = Send-NSRequest -RequestMethod Post -RequestTarget "tokens" -RequestArguments @{ 
    username = $networkCredential.UserName
    password = $networkCredential.Password
    app_name = $scriptName
  } -ErrorAction SilentlyContinue
  if (!$result.data.session_token -and !$ResetPassword) {
    $Script:ResetPassword = $true
    return Connect-NSGroup $NimbleGroup
  }
  else {
    $Global:session_token = $result.data.session_token
    return $result.data
  }
}

Function Send-NSRequest {
  [CmdletBinding()]
  Param
  (
    [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateSet('get', 'delete', 'post', 'put')]
    [string]$RequestMethod,

    [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
    [string]$RequestTarget,
        
    [Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)]
    [hashtable]$RequestArguments,

    [Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)]
    [hashtable]$RequestHeaders = @{},
        
    [Parameter(Mandatory = $false, ValueFromPipelineByPropertyName = $true)]
    [string]$NimbleGroup = $Script:NimbleGroup
  )

  if ( ![string]::IsNullOrEmpty($Global:session_token) ) { $RequestHeaders.Add("X-Auth-Token", $Global:session_token) }

  $uri = 'https://' + $NimbleGroup + ':5392/'
  if ($RequestTarget -eq 'versions') {
    $uri = $uri + 'versions'
  }
  else {
    $uri = $uri + 'v1/' + $RequestTarget
  }

  if ( $RequestMethod -ieq 'get' ) {
    if ($RequestArguments.Count -gt 0) {
      $uri += '?'
      $uri += [string]::join("&", @(foreach ($pair in $RequestArguments.GetEnumerator()) { 
            if ($pair.Name) { 
              $pair.Name + '=' + $pair.Value 
            } 
          }))
    }
  }
  else {
    $RequestJSON = @{ data = $RequestArguments } | ConvertTo-Json -Depth 5
  }

  try {
    $request = [System.Net.WebRequest]::Create($uri)
    $request.Method = $RequestMethod.ToUpper()
    $request.ContentType = "application/json"
    $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
    if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 ) {
      $requestStream = $request.GetRequestStream();
      $streamWriter = New-Object System.IO.StreamWriter $requestStream
      $streamWriter.Write($RequestJSON.ToString())
      $streamWriter.Flush()
      $streamWriter.Close()
    }
 
    $response = $request.GetResponse()
  }
  catch {
    $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
    if ( $httpStatus -eq '401' ) {
      Write-Error 'You must login into the Nimble Array first'
    }
    elseif ($httpStatus -ne $null) {
      $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
      $readStream = New-Object System.IO.StreamReader $responseStream
      $data = $readStream.ReadToEnd()
      $results = $data | ConvertFrom-Json
      Write-Error "$('There was an error, status code: ' + $httpStatus)`n$($uri)`n$($RequestJSON.ToString())`n$($results.messages | Format-Table | Out-String)"
    }
    else { 
      Write-Error $_.Exception
    }
    Return
  }
  $responseStream = $response.GetResponseStream()
  $readStream = New-Object System.IO.StreamReader $responseStream
  $data = $readStream.ReadToEnd()
  $results = New-Object -TypeName PSCustomObject
  $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
  $results | Add-Member -MemberType NoteProperty -Name data -Value $($($data | ConvertFrom-Json).data | ? {$_})

  Return $results
}

Function Get-StringHash {
  param(
    [Parameter(Mandatory = $true,
      ValueFromPipelineByPropertyName = $true)]
    [string]$String,
    [Parameter(Mandatory = $false,
      ValueFromPipelineByPropertyName = $true)]
    [string]$HashName = "MD5"
  )
  $StringBuilder = New-Object System.Text.StringBuilder
  [System.Security.Cryptography.HashAlgorithm]::Create($HashName).ComputeHash([System.Text.Encoding]::UTF8.GetBytes($String)) | % {
    [Void]$StringBuilder.Append($_.ToString("x2"))
  }
  $StringBuilder.ToString()
}

function Encrypt-String {
  param(
    $String, 
    $Passphrase, 
    $salt = "My Voice is my P455W0RD!", 
    $init = "Yet another key", 
    [switch]$arrayOutput
  )
  $r = new-Object System.Security.Cryptography.RijndaelManaged
  $pass = [Text.Encoding]::UTF8.GetBytes($Passphrase)
  $salt = [Text.Encoding]::UTF8.GetBytes($salt)

  $r.Key = (new-Object Security.Cryptography.PasswordDeriveBytes $pass, $salt, "SHA1", 5).GetBytes(32) #256/8
  $r.IV = (new-Object Security.Cryptography.SHA1Managed).ComputeHash( [Text.Encoding]::UTF8.GetBytes($init) )[0..15]
   
  $c = $r.CreateEncryptor()
  $ms = new-Object IO.MemoryStream
  $cs = new-Object Security.Cryptography.CryptoStream $ms, $c, "Write"
  $sw = new-Object IO.StreamWriter $cs
  $sw.Write($String)
  $sw.Close()
  $cs.Close()
  $ms.Close()
  $r.Clear()
  [byte[]]$result = $ms.ToArray()
  if ($arrayOutput) {
    return $result
  }
  else {
    return [Convert]::ToBase64String($result)
  }
}

function Decrypt-String {
  param(
    $Encrypted, 
    $Passphrase, 
    $salt = "My Voice is my P455W0RD!", 
    $init = "Yet another key"
  )
  if ($Encrypted -is [string]) {
    $Encrypted = [Convert]::FromBase64String($Encrypted)
  }

  $r = new-Object System.Security.Cryptography.RijndaelManaged
  $pass = [System.Text.Encoding]::UTF8.GetBytes($Passphrase)
  $salt = [System.Text.Encoding]::UTF8.GetBytes($salt)

  $r.Key = (new-Object Security.Cryptography.PasswordDeriveBytes $pass, $salt, "SHA1", 5).GetBytes(32) #256/8
  $r.IV = (new-Object Security.Cryptography.SHA1Managed).ComputeHash( [Text.Encoding]::UTF8.GetBytes($init) )[0..15]

  $d = $r.CreateDecryptor()
  $ms = new-Object IO.MemoryStream @(, $Encrypted)
  $cs = new-Object Security.Cryptography.CryptoStream $ms, $d, "Read"
  $sr = new-Object IO.StreamReader $cs
  Write-Output $sr.ReadToEnd()
  $sr.Close()
  $cs.Close()
  $ms.Close()
  $r.Clear()
}

Function Get-NSVolume {
  param(
    [Parameter(Mandatory = $false, ValueFromPipeline = $true)]
    [string[]]$VolumeID,
    [Parameter(Mandatory = $false)]
    [hashtable]$RequestArgument = @{}
  )
  Process {
    if ($VolumeID) {
      $requestTarget = "volumes/$VolumeID"
      $RequestArgument = @{}
    }
    else {
      $requestTarget = "volumes/detail"
    }
    (Send-NSRequest -RequestMethod get -RequestTarget $requestTarget -RequestArguments $RequestArgument).data 
  }
}

Function Set-NSVolume {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [PSCustomObject[]]$VolumeObject,
    [Parameter(Mandatory = $false)]
    [string]$VolumeId,
    [Parameter(Mandatory = $false)]
    [switch]$Online,
    [Parameter(Mandatory = $false)]
    [switch]$Force,
    [Parameter(Mandatory = $false)]
    [hashtable]$RequestArgument = @{},
    [Parameter(Mandatory = $false)]
    [hashtable]$Metadata
  )

  Process {
    if ($VolumeId) {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }
    if (!$VolumeObject.id) {
      Write-Error "A valid volume object is required!"
    }

    if ($PSBoundParameters.ContainsKey("Online")) { $RequestArgument.online = $Online.ToString().ToLower() }
    if ($PSBoundParameters.ContainsKey("Force")) { $RequestArgument.force = $Force.ToString().ToLower() }

    foreach ($key in $Metadata.Keys) {
      $RequestArgument.metadata += @(@{"key" = $key; "value" = $($Metadata.Item($key))})
    }

    $result = Send-NSRequest -RequestMethod put -RequestTarget $("volumes/" + $VolumeObject.id) -RequestArguments $RequestArgument
    $result.data
  }
}

Function Remove-NSVolume {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory = $false)]
    [string]$VolumeId
  )

  Process {
    if ($VolumeId) {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }
    if (!$VolumeObject.id) {
      Write-Error "A valid volume object is required!"
    }

    $result = try { Send-NSRequest -RequestMethod delete -RequestTarget $("volumes/" + $VolumeObject.id) } catch { @{ data = "Failed to delete $($VolumeObject.name)" } 
    }
    $result.data
  }
}

Function Get-NSSnapshot {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [string[]]$SnapshotID,
    [Parameter(Mandatory = $false)]
    [hashtable]$RequestArgument = @{}
  )
  Begin {
    $result = @()
  }
  Process {
    if ($SnapshotID) {
      $requestTarget = "snapshots/$SnapshotID"
      $RequestArgument = @{}
    }
    else {
      $requestTarget = "snapshots/detail"
    }
    $result += try { (Send-NSRequest -RequestMethod get -RequestTarget $requestTarget -RequestArguments $requestArgument).data } catch {}
  }
  End {
    Return $result | Sort-Object id
  }
}

Function New-NSSnapshot {
  param(
    [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
    [PSObject[]]$Volume,
    [Parameter(Mandatory = $true)]
    [string]$SnapshotName,
    [Parameter(Mandatory = $false)]
    [string]$Description,
    [Parameter(Mandatory = $false)]
    [hashtable]$Metadata = @{}
  )
  Begin {
    $requestArguments = @{ name = $SnapshotName; description = $Description }
    if ($Metadata.Count -gt 0) {
      foreach ($key in $Metadata.Keys) {
        $requestArguments.metadata += @(@{"key" = $key; "value" = $($Metadata.Item($key))})
      }
    }
  }
  Process {
    if ($Volume.id) {
      $volumeId = $Volume.id
    }
    else {
      $volumeId = $(Get-NSVolume -VolumeId $Volume).id
    }
    $requestArguments.vol_id = $volumeId
    return (Send-NSRequest -RequestMethod post -RequestTarget "snapshots" -RequestArguments $requestArguments).data
  }
}

Function Remove-NSSnapshot {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [System.Object[]]$SnapshotObject,
    [Parameter(Mandatory = $false)]
    [string]$VolumeId
  )

  Process {
    if ($SnapshotId) {
      $SnapshotObject = Get-NSVolume -VolumeId $SnapshotId
    }
    if (!$SnapshotObject.id) {
      Write-Error "A valid snapshot object or id is required!"
    }

    $result = try { Send-NSRequest -RequestMethod delete -RequestTarget $("snapshots/" + $SnapshotObject.id) } catch { @{ data = "Failed to remove snapshot $($SnapshotObject.name)" } 
    }
    $result.data
  }
}

Function New-NSSnapshotClone {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [System.Object[]]$SnapshotObject,
    [Parameter(Mandatory = $false)]
    [string]$SnapshotId,
    [Parameter(Mandatory = $false)]
    [string]$CloneName,
    [Parameter(Mandatory = $false)]
    [string]$Description,
    [Parameter(Mandatory = $false)]
    [hashtable]$Metadata
  )

  Process {
    if ($SnapshotId) {
      $SnapshotObject = Get-NSSnapshot -SnapshotId = $SnapshotId
    }
    elseif ($SnapshotObject.id -eq "") {
      Write-Error "Must supply a valid snapshot object or id!"
    }

    if (!$CloneName -and $SnapshotObject.vol_name) {
      $CloneName = $SnapshotObject.vol_name + "-Clone"
    }
    if (!$CloneName) { Write-Error "Must supply a valid clone name!" }

    $request = @{
      name         = $CloneName
      clone        = $true
      online       = $false
      base_snap_id = $SnapshotObject.id
    }
    if ($Description) { $request.Add("description", $Description) }
    foreach ($key in $Metadata.Keys) {
      $request.metadata += @(@{"key" = $key; "value" = $($Metadata.Item($key))})
    }

    $result = Send-NSRequest -RequestMethod post -RequestTarget "volumes" -RequestArguments $request
    $result.data
  }
}

Function Set-NSVolumeInitiatorGroup {
  param(
    [Parameter(Mandatory = $false,
      ValueFromPipeline = $true)]
    [System.Object[]]$VolumeObject,
    [Parameter(Mandatory = $false)]
    [string]$VolumeId,
    [Parameter(Mandatory = $false)]
    [string]$InitiatorGroupName
  )

  Process {
    if ($VolumeObject.id -eq "") {
      $VolumeObject = Get-NSVolume -VolumeId $VolumeId
    }

    $initiatorGroupObject = Get-NSInitiatorGroup -InitiatorGroup $InitiatorGroupName

    foreach ($acl in $VolumeObject.access_control_records) {
      $result = Send-NSRequest -RequestMethod delete -RequestTarget $("access_control_records/" + $acl.id)
    }

    
    $RequestArgument = @{
      "vol_id"             = $VolumeObject.id
      "initiator_group_id" = $initiatorGroupObject.id
    }

    $result = Send-NSRequest -RequestMethod post -RequestTarget "access_control_records/" -RequestArguments $RequestArgument
  }
}

Function Get-NSInitiatorGroup {
  param(
    [Parameter(Mandatory = $false)]
    [string]$InitiatorGroup,
    [Parameter(Mandatory = $false)]
    [string]$InitiatorGroupID,
    [Parameter(Mandatory = $false)]
    [hashtable]$RequestArgument = @{}
  )
  if ($InitiatorGroupID) {
    $requestTarget = "initiator_groups/$InitiatorGroupID"
  }
  else {
    $requestTarget = "initiator_groups/detail"
    if ($InitiatorGroup) { $requestArgument.name = $InitiatorGroup }
  }
  (Send-NSRequest -RequestMethod get -RequestTarget $requestTarget -RequestArguments $requestArgument).data
}

Main
