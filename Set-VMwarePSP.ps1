## <copyright file="Set-VMwarePSP.ps1" company="NimbleStorage">
## Copyright (c) 2013 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2013-03-12</date>
## <summary>Sets RoundRobin PSP rule for Nimble Storage volumes and applies policy to existing volumes</summary>

if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null) {
  try {
    Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
  }
  catch {
    Write-Host "Please install VMware PowerCLI"
    Return
  }
}

# Get VCMS server address
if ($args[0] -ne $null) {
  $vcms_ip = $args[0]
}
else {
  $vcms_ip = Read-Host "Please enter vCenter Server hostname or IP"
}

# Connect to VCMS and get list of hosts
try {
  Connect-VIServer -Server $vcms_ip
  $vmhosts = Get-VMHost -ErrorAction Stop
}
catch {
  Write-Host "Could not connect to vCenter (" $vcms_ip ") and get hosts" $?
  Return
}

# Loop through hosts and set MPIO options
foreach ($vmhost in $vmhosts) {
  Write-Host "Setting MPIO settings on" $vmhost.Name
  try {
    $esxcli = Get-EsxCli -VMhost $vmhost -ErrorAction Stop
  }
  catch {
    Write-Host "Could not open EsxCli session. Could be an ESX 4.x host."
    Continue
  }

  # Test if host is version 4 or 5
  if ($vmhost.Version -match "^4\.") {
    # ESX 4.1
    try {
      if ($esxcli.nmp.satp.addrule($null, $null, $null, $null, $null, $null, $null, "VMW_PSP_RR", $null, "VMW_SATP_ALUA", $null, "Nimble") -eq $true) {
        Write-Host "Added rule for Nimble volumes to use RoundRobin PSP"
      }
    }
    catch {
      Write-Host "Rule for Nimble volumes to use RoundRobin PSP is in place"
    }
      
    foreach ($lun in Get-ScsiLun -VmHost $vmhost | ? {$_.Vendor -eq "Nimble" }) {
      Write-Host "Setting MPIO settings for volume" $lun.CanonicalName
      $esxcli.nmp.device.setpolicy($null, $lun.CanonicalName, "VMW_PSP_RR") | Out-Null
      $esxcli.nmp.psp.setconfig("policy=iops;iops=1", $lun.CanonicalName, $null) | Out-Null
    }
  }
  elseif ($vmhost.Version -match "^5\.") {
    # ESX 5  
    try {
      $rule = $esxcli.storage.nmp.satp.rule.list() | ? {$_.Vendor -eq "Nimble"}
      #$esxcli.storage.nmp.satp.rule.remove($null, $null, $null, $null, $null, $null, $null, $null, $rule.DefaultPSP, $rule.PSPOptions, $rule.Name, $null, $null, $rule.Vendor)	
      if($esxcli.storage.nmp.satp.rule.remove($null, $null, $null, $null, $null, $null, $null, $rule.DefaultPSP, $rule.PSPOptions, $rule.Name, $null, $null, $rule.Vendor) -eq $true) {
        Write-Host "Removed rule for Nimble volumes to use RoundRobin PSP"
      }
    }
    catch {
      Write-Host "Couldn't remove old rule for Nimble volumes to use RoundRobin PSP"
    }
    try {
      if ($esxcli.storage.nmp.satp.rule.add($null, $null, $null, $null, $null, $null, $null, $null, "VMW_PSP_RR", "policy=iops;iops=1", "VMW_SATP_ALUA", $null, $null, "Nimble") -eq $true) {
        Write-Host "Added rule for Nimble volumes to use RoundRobin PSP"
      }
    }
    catch {
      Write-Host "Rule for Nimble volumes to use RoundRobin PSP is still in place"
    }
      
    foreach ($lun in Get-ScsiLun -VmHost $vmhost | ? {$_.Vendor -eq "Nimble" }) {
      Write-Host "Setting MPIO settings for volume" $lun.CanonicalName
      if ($vmhost.Version -match "^5\.0") {
        $esxcli.storage.nmp.device.set($null, $lun.CanonicalName, "VMW_PSP_RR") | Out-Null
        $esxcli.storage.nmp.psp.roundrobin.deviceconfig.set(262144, $lun.CanonicalName, 1, "iops", $null) | Out-Null
      }
      else {
        $esxcli.storage.nmp.device.set($false, $lun.CanonicalName, "VMW_PSP_RR") | Out-Null
        $esxcli.storage.nmp.psp.roundrobin.deviceconfig.set(262144, $null, $lun.CanonicalName, 1, "iops", $null) | Out-Null
      }
    } 
  }
  else {
    Write-Host "Could not validate ESX version for host" $vmhost.Name
  }
  Write-Host "----"
}

# Logout of the VCMS
Disconnect-VIServer -Confirm:$false -ErrorAction SilentlyContinue