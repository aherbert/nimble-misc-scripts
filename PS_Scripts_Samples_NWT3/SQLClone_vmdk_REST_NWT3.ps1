﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	12/2/2016 9:56 AM
	 Created by:   	dianneg
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		This script kicks of a clone workflow of volumes that house the vmdk's of a vm running SQL server. The script uses a combination of NWT3 cmdlets, 
		and REST API calls to complete the workflow. Once volumes are attached to host, script attaches SQL database.
#>

<#Import Modules necessary for script to run#>
Import-Module "NimblePowerShellToolkit"  <#The NimblePowerShell Toolkit is community supported.#>
Import-Module "sqlps" -DisableNameChecking
Add-PSSnapin VMware.VimAutomation.Core
ipmo 'C:\Program Files\Nimble Storage\bin\Nimble.Powershell.dll'

<# Variables for volume collections, arrays, vmware, and authentication#>
<#get creds : read-host -AsSecureString | ConvertFrom-SecureString | Out-File C:\Users\Administrator\Documents\nm_cred.txt#>
$nm_uid = "admin"
$nm_password = cat C:\Users\Administrator\Documents\nm_cred.txt | ConvertTo-SecureString
$nm_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nm_uid, $nm_password
$array_cs300 = "10.21.1.5"
$volcoll = "FC-SQL-05-Backup"
$vcred_uid = "administrator@vsphere.local"
$vcred_password = cat C:\Users\Administrator\Documents\vcred_pass.txt | ConvertTo-SecureString
$vcred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $vcred_uid, $vcred_password
$win_uid = "sedemo\administrator"
$win_password = cat C:\Users\Administrator\Documents\win_pass.txt | ConvertTo-SecureString
$wincred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $win_uid, $win_password
$dbds = "fc-sql-05-db-vmdk"
$tlds = "fc-sql-05-tl-vmdk"
$igroup_name = "ESXhosts"


<#Enable HTTPS: Service Point Manager cert validation callback#>

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

<#Function Definitions for REST. DO NOT EDIT#>
<#Authentication token for REST#>
function get-token
{
	param
	(
		[string]$array,
		[string]$uid,
		[string]$password
	)
	$data = @{
		username = $uid
		password = $password
	}
	
	$body = convertto-json (@{ data = $data })
	$uri = "https://" + $array + ":5392/v1/tokens"
	$token = Invoke-RestMethod -Uri $uri -Method Post -Body $body
	$token = $token.data.session_token
	return $token
}
<# Function to create new ACL on volume #>
function Create-ACL
{
	param
	(
		[string]$array,
		[string]$token,
		[string]$apply_to,
		[string]$vol_id,
		[string]$igroup_id
	)
	
	$data = @{
		apply_to = $apply_to
		initiator_group_id = $igroup_id
		vol_id = $vol_id
	}
	
	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $token }
	
	$uri = "https://" + $array + ":5392/v1/access_control_records"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
	return $result.data
}
<# Function to get the volume if for specific volume, so we can use the ID for other pruposes, ie. adding an ACL #>
function Get-volID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$volume
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/volumes"
	$volume_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	$vollist = $volume_list.data
	
	foreach ($vol in $vollist)
	{
		if ($vol.name -eq $volume)
		{
			$volid = $vol.id
			break
		}
	}
	Write-Output $volid
}
<# Function to get the igroup id for a specific igroup, so we can use the id for other purposes, ie. adding an ACL to a volume. #>
function Get-igroupID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$name
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/initiator_groups?name=" + $name
	$igroup_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	Write-Output $igroup_list.data.id
}
<# Function to get snapcollections by volume collection#>
function Get-NSSnapcolls
{
	param
	(
		[string]$volcoll
	)
	
	
	$header = @{ "X-Auth-Token" = $Global:tokenData.session_token }
	$uri = "https://" + $array + ":5392/v1/snapshot_collections/detail?volcoll_name=$volcoll"
	$result = Invoke-RestMethod -Uri $uri -Method Get -Body $body -Header $header
	
	Write-Output $result.data
}
<#Function to get latest snapcoll for given volcoll#>
function Get-NSLatestSnapColl
{
	param
	(
		[string]$volcoll
	)
	
	$vc = Get-NSVolumeCollection -name $volcoll
	$sc = Get-NSSnapcolls -volcoll $volcoll
	
	Write-Output $sc.snapshots_list | where name -like $vc.last_snapcoll.snapcoll_name
}
<# Function to clone volume collection #>
function Invoke-NSVolCollCloneLatest
{
	param
	(
		[string]$volcoll <#Parameter for Volume Collection Name#>
	)
	
	$latest_snap = Get-NSLatestSnapColl -volcoll $volcoll
	$snapcoll_vols = @()
	foreach ($vol in $latest_snap)
	{
		$base_snap_id = $vol.snap_id
		$name = $vol.vol_name + '-clone'
		
		$vol_clone = New-NSClone -name $name -base_snap_id $base_snap_id -online $false -clone $true
		
		Write-Output $vol_clone
	}
}
<##########################################>

<# Connect to Array and Set Group Mgmt IP list, connect to vcenter #>
Write-Host "Establishing Array Connections and Vcenter access"
Set-NWTConfiguration -GroupMgmtIP $array_cs300 -CredentialObj $nm_cred
Set-NWTConfiguration -GroupMgmtIPList $array_cs300
Connect-NSGroup -Credential $nm_cred -group $array_cs300
Connect-VIServer -Server vcenter6.sedemo.lab -Credential $vcred
$vmhost = "esxi5-7.sedemo.lab"


<# Clone Volumes from latest snapshot collection. Set variables for next steps#>
Write-Host "Cloning database from latest SnapshotCollection"
$cloned_vols = Invoke-NSVolCollCloneLatest -volcoll $volcoll
$dbds_clone = $cloned_vols | select-object -property name, access_control_records,serial_number | where { $_.name -like '*db*clone*' }
$tlds_clone = $cloned_vols | select-object -property name, access_control_records,serial_number | where { $_.name -like '*tl*clone*' }


<#Mount cloned volumes of database to VMware - leveraging rest API and Nimble PowerShell Toolkit and functions#>
Write-Host "Mounting cloned databases volumes to VMware"
$token = get-token -array $array_cs300 -uid $nm_uid -password $nm_cred.GetNetworkCredential().Password
$dbds_clone_id = Get-volID -array $array_cs300 -token $token -volume $dbds_clone.name
$tlds_clone_id = Get-volID -array $array_cs300 -token $token -volume $tlds_clone.name
$igroup_id = Get-igroupID -array $array_cs300 -token $token -name $igroup_name
Remove-NSAccessControlRecord -id $dbds_clone.access_control_records.id
Remove-NSAccessControlRecord -id $tlds_clone.access_control_records.id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $dbds_clone_id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $tlds_clone_id
Set-NSVolume -id $dbds_clone_id -online $true -read_only $false
Set-NSVolume -id $tlds_clone_id -online $true -read_only $false
Write-Host "Rescanning ESXi host for new devices"
VMware.VimAutomation.Core\Get-Cluster 'Cluster' | Get-VMHost -Name $vmhost | Get-VMHostStorage -RescanAllHba -RescanVmfs

Write-Host "Resignaturing volumes and adding as VMFS datastores"
$esxcli = Get-EsxCli -VMHost $vmhost
$snapdb = $esxcli.storage.vmfs.snapshot.list() | where { $_.VolumeName -match "$dbds" }
$snaptl = $esxcli.storage.vmfs.snapshot.list() | where { $_.VolumeName -match "$tlds" }
$esxcli.storage.vmfs.snapshot.resignature($null, $snapdb.VMFSUUID) | Out-Null
sleep 15
$esxcli.storage.vmfs.snapshot.resignature($null, $snaptl.VMFSUUID) | Out-Null
sleep 15

Write-Host "Renaming new VMFS datastores"
get-datastore | where { $_.Name -like "snap*$dbds" } | Set-Datastore -Name $dbds_clone.name
start-sleep -Seconds 10
get-datastore | where { $_.Name -like "snap*$tlds" } | Set-Datastore -Name $tlds_clone.name
start-sleep -Seconds 10

Write-Host "Adding VMDK to guest"
$vm = Get-VM FC-SQL-07
$datastoredb = $dbds_clone.name
$datastoretl = $tlds_clone.name
New-HardDisk -VM $vm -DiskPath "[$datastoredb] FC-SQL-05/FC-SQL-05.vmdk"
New-HardDisk -VM $vm -DiskPath "[$datastoretl] FC-SQL-05/FC-SQL-05.vmdk"

Write-Host "Mounting cloned databases to SQL Server"
<# Mount DB to SQL#>
Invoke-Command -ComputerName $vm -ScriptBlock {
	Import-Module "sqlps" -DisableNameChecking
	ipmo 'C:\Program Files\Nimble Storage\bin\Nimble.Powershell.dll'
	$db = "TPILNLABDB3_clone001"
	$mdfpath = "C:\SQLCLONE\NIMBLE\DB\MSSQL\TPILNLABDB\TPILNLABDB3.mdf"
	$ldfpath = "C:\SQLCLONE\NIMBLE\LOG\MSSQL\TPILNLABDB\TPILNLABDB3.ldf"
	Set-NimVolume -DiskID 1 -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
	mkdir c:\SQLCLONE\NIMBLE\DB
	sleep 5
	Add-PartitionAccessPath -DiskNumber 1 -PartitionNumber 1 -AccessPath 'c:\SQLCLONE\NIMBLE\DB' -ErrorAction SilentlyContinue
	Set-NimVolume -DiskID 2 -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
	mkdir c:\SQLCLONE\NIMBLE\LOG
	sleep 5
	Add-PartitionAccessPath -DiskNumber 2 -PartitionNumber 1 -AccessPath 'c:\SQLCLONE\NIMBLE\LOG' -ErrorAction SilentlyContinue
	$attachSQLCMD = @"
USE [master]
GO
CREATE DATABASE [$db] ON (FILENAME = '$mdfpath'),(FILENAME = '$ldfpath') for ATTACH
GO
"@
	Invoke-Sqlcmd $attachSQLCMD -QueryTimeout 3600 -ServerInstance 'FC-SQL-07' -Username "sa" -Password "Nim123Boli"
	Start-Sleep -Seconds 5
} -Credential $wincred

Write-Host "Cloning Complete, please check SQL Management Studio for new database"