﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	12/2/2016 11:08 AM
	 Created by:   	dianneg
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
	This script simulates the cloning of database volumes from a physical machine to a physical machine. As there was a ahrdware limitiation, the clone was attached back to the same physical machine. 
	But this can be easily changed by invoking this workflow from the destination machine. Leverages NWT3 and REST API functionality"
#>
<#Import Modules#>
Import-Module "sqlps" -DisableNameChecking
ipmo 'C:\Program Files\Nimble Storage\bin\Nimble.Powershell.dll'
Import-Module "NimblePowerShellToolkit"  <#The NimblePowerShell Toolkit is community supported.#>


<# Variables for volume collections, array authentication#>
<#get creds : read-host -AsSecureString | ConvertFrom-SecureString | Out-File C:\Users\Administrator\Documents\nm_cred.txt#>
$nm_uid = "admin"
$nm_password = cat C:\Users\Administrator\Documents\nm_cred.txt | ConvertTo-SecureString
$nm_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nm_uid, $nm_password
$win_uid = "sedemo\administrator"
$win_password = cat C:\Users\Administrator\Documents\win_pass.txt | ConvertTo-SecureString
$wincred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $win_uid, $win_password
$array_cs300 = "10.21.1.5"
$volcoll = "fc-sql-p-back"
$igroup_name = "SQL-P"

<#Enable HTTPS: Service Point Manager cert validation callback#>

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

<#Function Definitions for REST. DO NOT EDIT#>
<#Authentication token for REST#>
function get-token
{
	param
	(
		[string]$array,
		[string]$uid,
		[string]$password
	)
	$data = @{
		username = $uid
		password = $password
	}
	
	$body = convertto-json (@{ data = $data })
	$uri = "https://" + $array + ":5392/v1/tokens"
	$token = Invoke-RestMethod -Uri $uri -Method Post -Body $body
	$token = $token.data.session_token
	return $token
}
<# Function to create new ACL on volume #>
function Create-ACL
{
	param
	(
		[string]$array,
		[string]$token,
		[string]$apply_to,
		[string]$vol_id,
		[string]$igroup_id
	)
	
	$data = @{
		apply_to = $apply_to
		initiator_group_id = $igroup_id
		vol_id = $vol_id
	}
	
	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $token }
	
	$uri = "https://" + $array + ":5392/v1/access_control_records"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
	return $result.data
}
<# Function to get the volume if for specific volume, so we can use the ID for other pruposes, ie. adding an ACL #>
function Get-volID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$volume
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/volumes"
	$volume_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	$vollist = $volume_list.data
	
	foreach ($vol in $vollist)
	{
		if ($vol.name -eq $volume)
		{
			$volid = $vol.id
			break
		}
	}
	Write-Output $volid
}
<# Function to get the igroup id for a specific igroup, so we can use the id for other purposes, ie. adding an ACL to a volume. #>
function Get-igroupID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$name
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/initiator_groups?name=" + $name
	$igroup_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	Write-Output $igroup_list.data.id
}
<# Function to get snapcollections by volume collection#>
function Get-NSSnapcolls
{
	param
	(
		[string]$volcoll
	)
	
	
	$header = @{ "X-Auth-Token" = $Global:tokenData.session_token }
	$uri = "https://" + $array + ":5392/v1/snapshot_collections/detail?volcoll_name=$volcoll"
	$result = Invoke-RestMethod -Uri $uri -Method Get -Body $body -Header $header
	
	Write-Output $result.data
}
<#Function to get latest snapcoll for given volcoll#>
function Get-NSLatestSnapColl
{
	param
	(
		[string]$volcoll
	)
	
	$vc = Get-NSVolumeCollection -name $volcoll
	$sc = Get-NSSnapcolls -volcoll $volcoll
	
	Write-Output $sc.snapshots_list | where name -like $vc.last_snapcoll.snapcoll_name
}
<# Function to clone volume collection #>
function Invoke-NSVolCollCloneLatest
{
	param
	(
		[string]$volcoll <#Parameter for Volume Collection Name#>
	)
	
	$latest_snap = Get-NSLatestSnapColl -volcoll $volcoll
	$snapcoll_vols = @()
	foreach ($vol in $latest_snap)
	{
		$base_snap_id = $vol.snap_id
		$name = $vol.vol_name + '-clone'
		
		$vol_clone = New-NSClone -name $name -base_snap_id $base_snap_id -online $false -clone $true
		
		Write-Output $vol_clone
	}
}
<##########################################>

<# Connect to Array and Set Group Mgmt IP list#>
Write-Host "Establishing Array Connections"
Set-NWTConfiguration -GroupMgmtIP $array_cs300 -CredentialObj $nm_cred
Set-NWTConfiguration -GroupMgmtIPList $array_cs300
Connect-NSGroup -Credential $nm_cred -group $array_cs300

<# Clone Volumes from latest snapshot collection. Set variables for next steps#>
Write-Host "Cloning database from latest SnapshotCollection"
$cloned_vols = Invoke-NSVolCollCloneLatest -volcoll $volcoll
$dbds_clone = $cloned_vols | select-object -property name, access_control_records, serial_number | where { $_.name -like '*db*clone*' }
$tlds_clone = $cloned_vols | select-object -property name, access_control_records, serial_number | where { $_.name -like '*tl*clone*' }

<#Mount cloned volumes of database to physical machine#>
Write-Host "Mounting cloned databases volumes"
$token = get-token -array $array_cs300 -uid $nm_uid -password $nm_cred.GetNetworkCredential().Password
$dbds_clone_id = Get-volID -array $array_cs300 -token $token -volume $dbds_clone.name
$tlds_clone_id = Get-volID -array $array_cs300 -token $token -volume $tlds_clone.name
$igroup_id = Get-igroupID -array $array_cs300 -token $token -name $igroup_name
Remove-NSAccessControlRecord -id $dbds_clone.access_control_records.id
Remove-NSAccessControlRecord -id $tlds_clone.access_control_records.id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $dbds_clone_id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $tlds_clone_id
Set-NSVolume -id $dbds_clone_id -online $true -read_only $false
Set-NSVolume -id $tlds_clone_id -online $true -read_only $false
Update-HostStorageCache

<#Rescan for new storage and add Access Paths#>
Get-NimVolume -OutVariable nim_volumes
$dbds_serial = $nim_volumes | select-object -property NimbleVolumeName, SerialNumber | where { $_.NimbleVolumeName -like '*db*clone*' }
$tlds_serial = $nim_volumes | select-object -property NimbleVolumeName, SerialNumber | where { $_.NimbleVolumeName -like '*tl*clone*' }
Set-NimVolume -SerialNumber $dbds_serial.SerialNumber -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
Set-NimVolume -SerialNumber $tlds_serial.SerialNumber -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
$dbds_disk_number = Get-Disk | Where-Object -FilterScript { $_.SerialNumber -eq $dbds_serial.SerialNumber } | Select-Object -Property Number
$tlds_disk_number = Get-Disk | Where-Object -FilterScript { $_.SerialNumber -eq $tlds_serial.SerialNumber } | Select-Object -Property Number
mkdir C:\SQLCLONE\NIMBLE\DB
mkdir C:\SQLCLONE\NIMBLE\LOG
Add-PartitionAccessPath -DiskNumber $dbds_disk_number.Number -PartitionNumber 1 -AccessPath 'C:\SQLCLONE\NIMBLE\DB' -ErrorAction SilentlyContinue
Add-PartitionAccessPath -DiskNumber $tlds_disk_number.Number -PartitionNumber 1 -AccessPath 'C:\SQLCLONE\NIMBLE\LOG' -ErrorAction SilentlyContinue

<#Mounting DB to SQL#>
Write-Host "Mounting cloned databases to SQL Server"
$db = "TPILNLABDB4_clone001"
$mdfpath = "c:\SQLCLONE\NIMBLE\DB\MSSQL\TPILNLABDB\TPILNLABDB4.mdf"
$ldfpath = "c:\SQLCLONE\NIMBLE\LOG\MSSQL\TPILNLABDB\TPILNLABDB4.ldf"

$attachSQLCMD = @"
USE [master]
GO
CREATE DATABASE [$db] ON (FILENAME = '$mdfpath'),(FILENAME = '$ldfpath') for ATTACH
GO
"@
Invoke-Sqlcmd $attachSQLCMD -QueryTimeout 3600 -ServerInstance 'SQL-P' -Username "sa" -Password "Nim123Boli"
Start-Sleep -Seconds 5

Write-Host "Cloning Complete, please check SQL Management Studio for new database"










