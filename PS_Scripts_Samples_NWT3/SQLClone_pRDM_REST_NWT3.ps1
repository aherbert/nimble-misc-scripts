﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	12/2/2016 11:06 AM
	 Created by:   	dianneg
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		This scrpt clones volumes housing SQL data from a physical machine to a VM, connecting as pRDM, and attaches SQL database. Leverages NWT3 functionality and REST API.
#>

<#Import Modules#>
Import-Module "NimblePowerShellToolkit"  <#The NimblePowerShell Toolkit is community supported.#>
Import-Module "sqlps" -DisableNameChecking
ipmo 'C:\Program Files\Nimble Storage\bin\Nimble.Powershell.dll'
Add-PSSnapin VMware.VimAutomation.Core

<# Variables for volume collections, arrays, vmware, and authentication#>
<#get creds : read-host -AsSecureString | ConvertFrom-SecureString | Out-File C:\Users\Administrator\Documents\nm_cred.txt#>
$nm_uid = "admin"
$nm_password = cat C:\Users\Administrator\Documents\nm_cred.txt | ConvertTo-SecureString
$nm_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nm_uid, $nm_password
$vcred_uid = "administrator@vsphere.local"
$vcred_password = cat C:\Users\Administrator\Documents\vcred_pass.txt | ConvertTo-SecureString
$vcred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $vcred_uid, $vcred_password
$win_uid = "sedemo\administrator"
$win_password = cat C:\Users\Administrator\Documents\win_pass.txt | ConvertTo-SecureString
$wincred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $win_uid, $win_password
$array_cs300 = "10.21.1.5"
$volcoll = "fc-sql-p-back"
$dbds = "fc-sql-p-db"
$tlds = "fc-sql-p-tl"
$app_obj = "\SQL-P\TPILNLABDB4"
$igroup_name = "ESXhosts"

<#Enable HTTPS: Service Point Manager cert validation callback#>

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

<#Function Definitions for REST. DO NOT EDIT#>
<#Authentication token for REST#>
function get-token
{
	param
	(
		[string]$array,
		[string]$uid,
		[string]$password
	)
	$data = @{
		username = $uid
		password = $password
	}
	
	$body = convertto-json (@{ data = $data })
	$uri = "https://" + $array + ":5392/v1/tokens"
	$token = Invoke-RestMethod -Uri $uri -Method Post -Body $body
	$token = $token.data.session_token
	return $token
}
<# Function to create new ACL on volume #>
function Create-ACL
{
	param
	(
		[string]$array,
		[string]$token,
		[string]$apply_to,
		[string]$vol_id,
		[string]$igroup_id
	)
	
	$data = @{
		apply_to = $apply_to
		initiator_group_id = $igroup_id
		vol_id = $vol_id
	}
	
	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $token }
	
	$uri = "https://" + $array + ":5392/v1/access_control_records"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
	return $result.data
}
<# Function to get the volume if for specific volume, so we can use the ID for other pruposes, ie. adding an ACL #>
function Get-volID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$volume
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/volumes"
	$volume_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	$vollist = $volume_list.data
	
	foreach ($vol in $vollist)
	{
		if ($vol.name -eq $volume)
		{
			$volid = $vol.id
			break
		}
	}
	Write-Output $volid
}
<# Function to get the igroup id for a specific igroup, so we can use the id for other purposes, ie. adding an ACL to a volume. #>
function Get-igroupID
{
	param
	(
		[string]$token,
		[string]$array,
		[string]$name
	)
	
	$header = @{ "X-Auth-Token" = $token }
	$uri = "https://" + $array + ":5392/v1/initiator_groups?name=" + $name
	$igroup_list = Invoke-RestMethod -Uri $uri -Method Get -Header $header
	
	Write-Output $igroup_list.data.id
}
<# Function to get snapcollections by volume collection#>
function Get-NSSnapcolls
{
	param
	(
		[string]$volcoll
	)
	
	
	$header = @{ "X-Auth-Token" = $Global:tokenData.session_token }
	$uri = "https://" + $array + ":5392/v1/snapshot_collections/detail?volcoll_name=$volcoll"
	$result = Invoke-RestMethod -Uri $uri -Method Get -Body $body -Header $header
	
	Write-Output $result.data
}
<#Function to get latest snapcoll for given volcoll#>
function Get-NSLatestSnapColl
{
	param
	(
		[string]$volcoll
	)
	
	$vc = Get-NSVolumeCollection -name $volcoll
	$sc = Get-NSSnapcolls -volcoll $volcoll
	
	Write-Output $sc.snapshots_list | where name -like $vc.last_snapcoll.snapcoll_name
}
<# Function to clone volume collection #>
function Invoke-NSVolCollCloneLatest
{
	param
	(
		[string]$volcoll <#Parameter for Volume Collection Name#>
	)
	
	$latest_snap = Get-NSLatestSnapColl -volcoll $volcoll
	$snapcoll_vols = @()
	foreach ($vol in $latest_snap)
	{
		$base_snap_id = $vol.snap_id
		$name = $vol.vol_name + '-clone'
		
		$vol_clone = New-NSClone -name $name -base_snap_id $base_snap_id -online $false -clone $true
		
		Write-Output $vol_clone
	}
}
<##########################################>

<# Connect to Array and Set Group Mgmt IP list, connect to vcenter #>
Write-Host "Establishing Array Connections and Vcenter access"
Set-NWTConfiguration -GroupMgmtIP $array_cs300 -CredentialObj $nm_cred
Set-NWTConfiguration -GroupMgmtIPList $array_cs300
Connect-NSGroup -Credential $nm_cred -group $array_cs300
Connect-VIServer -Server vcenter6.sedemo.lab -Credential $vcred
$vmhost = "esxi5-7.sedemo.lab"

<# Clone Volumes from latest snapshot collection. Set variables for next steps#>
Write-Host "Cloning database from latest SnapshotCollection"
$cloned_vols = Invoke-NSVolCollCloneLatest -volcoll $volcoll
$dbds_clone = $cloned_vols | select-object -property name, access_control_records, serial_number | where { $_.name -like '*db*clone*' }
$tlds_clone = $cloned_vols | select-object -property name, access_control_records, serial_number | where { $_.name -like '*tl*clone*' }

<#Mount cloned volumes of database to VMware#>
Write-Host "Mounting cloned databases volumes to VMware"
$token = get-token -array $array_cs300 -uid $nm_uid -password $nm_cred.GetNetworkCredential().Password
$dbds_clone_id = Get-volID -array $array_cs300 -token $token -volume $dbds_clone.name
$tlds_clone_id = Get-volID -array $array_cs300 -token $token -volume $tlds_clone.name
$igroup_id = Get-igroupID -array $array_cs300 -token $token -name $igroup_name
Remove-NSAccessControlRecord -id $dbds_clone.access_control_records.id
Remove-NSAccessControlRecord -id $tlds_clone.access_control_records.id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $dbds_clone_id
create-ACL -apply_to "both" -array $array_cs300 -token $token -igroup_id $igroup_id -vol_id $tlds_clone_id
Set-NSVolume -id $dbds_clone_id -online $true -read_only $false
Set-NSVolume -id $tlds_clone_id -online $true -read_only $false

Write-Host "Rescanning ESXi host for new devices"
Get-Cluster 'Cluster' | Get-VMHost -Name $vmhost | Get-VMHostStorage -RescanAllHba -RescanVmfs

Write-Host "Adding RDM to Guest"
$vm = Get-VM FC-SQL-07
$snapdb = Get-ScsiLun -VmHost $vmhost | where { $_.CanonicalName -match "eui." + $dbds_clone.serial_number }
$snaptl = Get-ScsiLun -VmHost $vmhost | where { $_.CanonicalName -match "eui." + $tlds_clone.serial_number }
New-HardDisk -VM $vm -DiskType RawPhysical -DeviceName $snapdb.ConsoleDeviceName
New-HardDisk -VM $vm -DiskType RawPhysical -DeviceName $snaptl.ConsoleDeviceName
sleep 25

Write-Host "Mounting cloned databases to SQL Server"
<# Mount DB to SQL#>
Invoke-Command -ComputerName $vm -ScriptBlock {
	Import-Module "sqlps" -DisableNameChecking
	ipmo 'C:\Program Files\Nimble Storage\bin\Nimble.Powershell.dll'
	$db = "TPILNLABDB4_clone001"
	$mdfpath = "c:\SQLCLONE\NIMBLE\DB3\MSSQL\TPILNLABDB\TPILNLABDB4.mdf"
	$ldfpath = "c:\SQLCLONE\NIMBLE\LOG3\MSSQL\TPILNLABDB\TPILNLABDB4.ldf"
	$nm_uid = "admin"
	$nm_password = cat C:\Users\Administrator\Documents\nm_cred.txt | ConvertTo-SecureString
	$nm_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nm_uid, $nm_password
	$win_uid = "sedemo\administrator"
	$win_password = cat C:\Users\Administrator\Documents\win_pass.txt | ConvertTo-SecureString
	$wincred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $win_uid, $win_password
	$array_cs300 = "10.21.1.5"
	Set-NWTConfiguration -GroupMgmtIP $array_cs300 -CredentialObj $nm_cred
	Set-NWTConfiguration -GroupMgmtIPList $array_cs300
	Get-NimVolume -OutVariable nim_volumes
	$dbds_serial = $nim_volumes | select-object -property NimbleVolumeName, SerialNumber | where { $_.NimbleVolumeName -like '*db*clone*' }
	$tlds_serial = $nim_volumes | select-object -property NimbleVolumeName, SerialNumber | where { $_.NimbleVolumeName -like '*tl*clone*' }
	Set-NimVolume -SerialNumber $dbds_serial.SerialNumber -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
	Set-NimVolume -SerialNumber $tlds_serial.SerialNumber -ReadOnly $false -Hidden $false -Online $true -NoDefaultDriveLetter $true
	$dbds_disk_number = Get-Disk | Where-Object -FilterScript { $_.SerialNumber -eq $dbds_serial.SerialNumber } | Select-Object -Property Number
	$tlds_disk_number = Get-Disk | Where-Object -FilterScript { $_.SerialNumber -eq $tlds_serial.SerialNumber } | Select-Object -Property Number
	mkdir C:\SQLCLONE\NIMBLE\DB3
	mkdir C:\SQLCLONE\NIMBLE\LOG3
	Add-PartitionAccessPath -DiskNumber $dbds_disk_number.Number -PartitionNumber 1 -AccessPath 'C:\SQLCLONE\NIMBLE\DB3' -ErrorAction SilentlyContinue
	Add-PartitionAccessPath -DiskNumber $tlds_disk_number.Number -PartitionNumber 1 -AccessPath 'C:\SQLCLONE\NIMBLE\LOG3' -ErrorAction SilentlyContinue
	$attachSQLCMD = @"
USE [master]
GO
CREATE DATABASE [$db] ON (FILENAME = '$mdfpath'),(FILENAME = '$ldfpath') for ATTACH
GO
"@
	Invoke-Sqlcmd $attachSQLCMD -QueryTimeout 3600 -ServerInstance 'FC-SQL-07' -Username "sa" -Password "Nim123Boli"
	Start-Sleep -Seconds 5
} -Credential $wincred

Write-Host "Cloning Complete, please check SQL Management Studio for new database"




