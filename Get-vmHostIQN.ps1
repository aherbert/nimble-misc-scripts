## <copyright file="Get-vmHostIQN.ps1" company="NimbleStorage">
## Copyright (c) 2013 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2015-07-27</date>
## <summary>Gets the list of vmHosts per cluster with each iSCSI IQN</summary>


if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null) {
  try {
    Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
  }
  catch {
    Write-Host "Please install VMware PowerCLI"
    Return
  }
}

# Get VCMS server address
if ($args[0] -ne $null) {
  $vCenterServer = $args[0]
}
else {
  $vCenterServer = Read-Host "Please enter vCenter Server hostname or IP"
}

Connect-VIServer $vCenterServer

$vmHostData = @()
ForEach ($Cluster in Get-Cluster) {
  ForEach ($vmHost in ($Cluster | Get-VMHost)) {
    $vmHostView = $vmHost | Get-View
    $vmHostStorageView = Get-View $vmHostView.ConfigManager.StorageSystem
    $vmHostSummary = “” | Select ClusterName, HostName, IQN
    $vmHostSummary.HostName = $vmHost.Name
    $vmHostSummary.ClusterName = $Cluster.Name
    $vmHostSummary.IQN = $vmHostStorageView.StorageDeviceInfo.HostBusAdapter.iScsiName
    $myHostData += $vmHostSummary
  }
}
$myHostData