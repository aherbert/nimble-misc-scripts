### CONFIG VARIABLES ###
$ArrayIP = "10.10.10.10"
$ArrayUsername = "admin"
$ArrayPassword = "admin"
# VolumeCollection is the Volume Collection that has the Database and log volume you need to mount
$VolumeCollection = "SQLServer"
# IntiatorGroup is the Intiator group that you want set for access to the cloned volumes
$InitiatorGroup = "Win2k8r2-01Snap"
# VolumeID is a made up value that we stuff in to make sure we only delete a volume created with this script
$VolumeID = "11111"

# Get install Path
$myPath = Split-Path $MyInvocation.MyCommand.Path

# Build Nimble GroupMgmt Object
[Reflection.Assembly]::LoadFile($myPath+"\GroupMgmt.dll") | Out-Null
$gm = New-Object GroupMgmt

# Import iSCSI Cmdlets
. ($myPath+"\iSCSI_Cmdlets.ps1")

# Set Array Management URL
$gm.Url = "http://"+$ArrayIP+":4210/soap" 

# Login to the array
$sid = [ref]""
$return = $gm.login($ArrayUsername, $ArrayPassword, $sid)
if ($return -eq "SMok") {
  Write-Host "Logged into array @ "+$gm.Url
}
else {
  Write-Host "Couldn't login to "$gm.Url
}

# Get Snapshot list for Volume Collection
$snapCollList = New-Object SnapCollection
$return = $gm.getSnapCollectionList($sid.Value, $VolumeCollection, [ref]$snapCollList)
if ($return -eq "SMok" -and $snapCollList.Count -gt 0) {
  Write-Host "Found "$snapCollList.Count" snapshots"
}
else {
  Write-Host "No snapshots in "$VolumeCollection
  Exit
}

# Get top snapshot
$snapColl = $snapCollList[0]
Write-Host "Using most recent snapshot "$snapColl.name" with the following volumes:"
$snapColl.snapList | Format-Table Vol

# Loop through each volume
foreach($snap in $snapColl.snapList) {

 # Set Volume Variables
 $myVolName = $snap.vol + "-" + $env:COMPUTERNAME + "-" + $VolumeID
 $myDescription = "Auto clone for " + $snap.vol + " to " + $env:COMPUTERNAME + ":" + $SQLInstance

 # Delete any Existing Auto Clones
 $volList = New-Object Vol
 $return = $gm.getVolList($sid.Value, [ref]$volList)
 
 foreach ($vol in $volList | ? {$_.name -eq $myVolName -and `
                                $_.description -eq $myDescription -and `
								$_.clone -eq $true}) {
  # TODO - put more sanity checking here - DON'T DELETE THE WRONG VOLUME
  $return = $gm.onlineVol($sid.Value, $vol.name, $false)
  if ($return -eq "SMok") {
   Write-Host "Set old volume " $vol.name " offline"
  }
  else {
   Write-Host "Could not set old volume " $vol.name " offline"
   Exit
  }
  
  $return = $gm.deleteVol($sid.Value, $vol.name)
  if ($return -eq "SMok") {
   Write-Host "Deleted old volume " $vol.name
  }
  else {
   Write-Host "Could not delete old volume " $vol.name
   Exit
  }

 }
 
 # Set Volume Attributes
 $attr = New-Object VolCreateAttr
 $attr.warnlevel = $snap.size * .8
 $attr.quota = $snap.size
 $attr.snapquota = 9223372036854775807
 $attr.name = $myVolName
 $attr.description = $myDescription
 $attr.online = $true

 # Clone Volume

 $return = $gm.cloneVol($sid.Value, $snap.vol, $snap.name, $attr)
 if ($return -eq "SMok") {
  Write-Host "Created clone for " $snap.vol " as " $attr.name
 }
 else {
  Write-Host "Could not create new clone for" $snap.vol "as" $attr.name
  Exit
 }

 # Set ACL
 $return = $gm.addVolAcl($sid.Value, $myVolName, "SMvolaclapplytoboth", "*", $InitiatorGroup)
 if ($return -eq "SMok") {
  Write-Host "Added " $InitiatorGroup " to " $myVolName " ACL list"
 }
 else {
  Write-Host "Could not add ACL to " $myVolName
  Exit
 }
}

# Refresh iSCSI Targets
Write-Host "Refreshing to find new targets"
Refresh-Targets

# Get Available Nimble Targets
$NimbleTargets = Get-AvailableTargets | ? {$_.Target -notmatch "com.nimblestorage:control-"} | ? {$_.Target -match "com.nimblestorage"} | Sort-Object -Unique Target
Write-Host "Found " $NimbleTargets.Count " Nimble targets"

# Loop through each Nimble target and host IP and validate connections. If any are missing, execute iscsicli to connect and make persistent 
Write-Host "Removing all old Nimble persistent iSCSI logins"
foreach ($Login in Get-PersistentLogins | ? {$_.Target -match "com.nimblestorage"}) {
  if ($Login.Target -eq $null) {
    continue
  }
  iscsicli removepersistenttarget $Login.Initiator $Login.Target $Login.InitiatorPort $Login.TargetIP $Login.TargetPort | Out-Null
}
foreach ($Target in $NimbleTargets) {
  Write-Host "Connecting to " $Target.Target
  Connect-toIQN $Target.Target
}

# Wait 5 seconds for connections to finalize
Write-Host "Rescanning for new drives"
"rescan `n exit" | diskpart | out-null
Start-Sleep -Seconds 10


# Get current set of Nimble sessions
$NimbleDisks = Get-EstablishedSessions | ? {$_.Target -match "com.nimblestorage"} | ? {$_.Target -match $env:COMPUTERNAME + "-" + $SQLInstance } | Select Target,TargetNice,Devices -Unique

# Loop Nimble Disks and make sure they are online and mount if not already mounted
foreach ($NimbleDisk in $NimbleDisks) {
  Write-Host "Setting " $NimbleDisk.TargetNice " online"
  "select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n attributes disk clear readonly noerr`n online `n online disk" | diskpart | out-null
  $Disk = Get-WmiObject -Class Win32_DiskDrive | ? {$_.Caption -match "Nimble Server" -and $_.DeviceID -eq $NimbleDisk.Devices[0].LegacyName}
  $Partitions = Get-WmiObject -Query "ASSOCIATORS OF {$($Disk.Path.RelativePath)}" | ? {$_.__CLASS -eq "Win32_DiskPartition"}
  foreach ($Partition in $Partitions) {
	$Path = "c:\nimble\"+$NimbleDisk.TargetNice+"\"+$Partition.Index
	if(Test-Path -Path $Path -PathType Container) {
	  mountvol $Path /d
	}
	else {
	  New-Item $Path -ItemType Directory | Out-Null
	}
	Write-Host "Mounting " $NimbleDisk.TargetNice " @ " $Path
	"select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n select partition "+$($Partition.Index + 1)+"`n attributes volume clear readonly noerr`n attributes volume clear hidden noerr`n attributes volume clear shadowcopy noerr`n assign mount="+$Path | diskpart | out-null
	chkdsk /x $Path | out-null
  }
}

# Restart SQL Services
foreach($service in $services) {
 if((Get-WmiObject Win32_Service -filter ("Name='"+$service.Name+"'")).StartMode -eq "Auto") {
  Write-Host "Starting SQL service " $service.Name
  Start-Service -Name $service.Name
 }
}

Write-Host "Finished!"

$return = $gm.logout($sid.Value) 

