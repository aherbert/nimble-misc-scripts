## <copyright file="Start-NimbleVDBench.ps1" company="NimbleStorage">
## Copyright (c) 2013 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2015-01-19</date>
## <summary>Automates the AFA VDBench toolkit</summary>

### CONFIG
$nimbleArray = "172.20.6.11"
$nimbleUser = "admin"
$nimblePassword = "admin"
$initiatorGroup = "IAD-LAB-ESX-XX"
$vcms_ip = "172.20.6.20"
$afa_model = "AF-7000"
$plink = "C:\Program Files (x86)\PuTTY\plink.exe"
$vdb_command_host = "172.20.6.100"

### END CONFIG

### SATIC VARS

$volumeSize = @{
    'AF-7000' = '819200'
    'AF-9000' = '1638400'
}

###

# Functions

Function Reset-Workers
{
    param
    (
        [array]$vdbWorkers,
        [switch]$RemoveHardDisks,
        [switch]$AddHardDisks,
        [switch]$StartVM
    )
    
    if($RemoveHardDisks.IsPresent) 
    {
        #Shutdown Worker Nodes
        Stop-VM -VM $vdbWorkers -Confirm:$false | Out-Null

        # Remove RDM disks
        foreach ($vm in $vdbWorkers)
        {
            $rdmList = Get-HardDisk -vm $vm | ? { $_.DiskType -like "Raw*" }
            Remove-HardDisk -HardDisk $rdmList -DeletePermanently -Confirm:$false | Out-Null
        }
    }
    elseif($AddHardDisks.IsPresent)
    {
        Get-Cluster | Get-VMHost | Get-VMHostStorage -RescanAllHba | Out-Null
        Start-Sleep -Seconds 30

        $vdbVolumes = Get-NimbleVolume | ? { $_.name -ilike "vdbench*" } | Sort-Object -Property name
        $mappedDisks = $vdbWorkers | Get-HardDisk -DiskType "RawPhysical","RawVirtual" | Select ScsiCanonicalName
        $i = 1
        foreach ($volume in $vdbVolumes)
        {
            if ($mappedDisks | Where-Object {$_.ScsiCanonicalName -eq "eui.$($volume.serial_number)"}) { Continue }
            $scsiLun = Get-ScsiLun -VmHost $vmhosts[0] -CanonicalName "eui.$($volume.serial_number)"
            New-HardDisk -VM $vdbWorkers[$i % $vdbWorkers.Count] -DiskType RawVirtual -DeviceName $scsiLun.ConsoleDeviceName | Out-Null
            $i++
        }
        if ($StartVM.IsPresent) 
        {
            Start-VM -VM $vdbWorkers | Out-Null
        }
    }
}

Function Clean-NimbleVolumes
{
    #Delete all vdbench volumes from AFA
    Connect-NimbleArray -array $nimbleArray -UserName $nimbleUser -Password $nimblePassword
    $vdbVolumes = Get-NimbleVolume | ? { $_.name -ilike "vdbench*" }
    foreach ($vol in $vdbVolumes)
    {
        Offline-NimbleVolume -Volume $vol.name
        Set-NimbleVolume -Volume $vol.name -StopProtecting
        Remove-NimbleVolume -Volume $vol.name
    }
}

# Nimble API Functions
function Connect-NimbleArray
{

    param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $array,

        # Param2 help description
        [string]
        $UserName="admin",

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]
        $Password
    )

    $jsonData = @{
		username = $username
		password = $password
	}
	
	$body = convertto-json (@{ data = $jsonData })
	
	$uri = "https://" + $array + ":5392/v1/tokens"
	$token = Invoke-RestMethod -Uri $uri -Method Post -Body $body 
	
    $script:token = $token.data.session_token
    $script:array = $array
}

Function Get-NimbleVolume
{
    param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Volume
    )

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/volumes/detail"
    if ($Volume -ne $null)
    {
        $uri = $uri + "?name=" + $Volume
    }

    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $header

    return $response.data
}

function New-NimbleVolume
{
	param
	(
		[Parameter(Mandatory=$true)][string]$VolumeName,
		[Parameter(Mandatory=$true)][int]$VolumeSizeMB,
        [Parameter(Mandatory=$false)][string]$PerformancePolicy,
        [switch]$Encrypt
   	)
	
	$data = @{
		name = $VolumeName
		size = $VolumeSizeMB
		online = "true"
	}

    if($Encrypt.IsPresent)
    {
        $data.Add("encryption_cipher", "aes_256_xts")
    }

    if ($PerformancePolicy -ne $null)
    {
        $PerfPolObj = Get-NimblePerformancePolicy $PerformancePolicy
        $data.Add("perfpolicy_id", $PerfPolObj.id)
    }

	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $script:token }
	$uri = "https://" + $script:array + ":5392/v1/volumes"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
}

Function Remove-NimbleVolume
{
    param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string]$Volume
    )

    $NimbleVolume = Get-NimbleVolume -Volume $Volume

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/volumes/" + $NimbleVolume.id

    $response = Invoke-RestMethod -Uri $uri -Method Delete -Headers $header
}

Function Offline-NimbleVolume
{
    param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [string]$Volume
    )

    $NimbleVolume = Get-NimbleVolume -Volume $Volume

    $data = @{
        force = "true"
        online = "false"
    }
    $data = ConvertTo-Json(@{data=$data})

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/volumes/" + $NimbleVolume.id

    $response = Invoke-RestMethod -Uri $uri -Method Put -Headers $header -Body $data
}

Function New-NimbleVolumeACL
{
	param
	(
		[Parameter(Mandatory=$true)][string]$Volume,
		[Parameter(Mandatory=$false)][string]$ApplyTo="both",
		[Parameter(Mandatory=$false)][string]$CHAPUser,
        [Parameter(Mandatory=$false)][string]$InitiatorGroup,
        [Parameter(Mandatory=$false)][int]$LUN=-1
	)
	
    $NimbleVolume = Get-NimbleVolume $Volume

	$data = @{
		vol_id = $NimbleVolume.id
        apply_to = $ApplyTo
	}

    #TODO if($CHAPUser -ne $null) { }

    if($InitiatorGroup -ne $null) {
        $InitiatorGroupObj = Get-InitiatorGroup $InitiatorGroup
        $data.Add("initiator_group_id", $InitiatorGroupObj.id)
    }

    if($LUN -gt -1) { $data.Add("lun", $LUN) }
	
	$body = ConvertTo-Json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $script:token }
	$uri = "https://" + $script:array + ":5392/v1/access_control_records"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header
}

Function Remove-NimbleVolumeACL
{
    param
    (
        [Parameter(Mandatory=$true)][string]$Volume,
        [string]$InitiatorGroup,
        [switch]$RemoveAll
    )

    if($InitiatorGroup -ne "")
    {
        $NimbleACL = Get-NimbleVolumeACL -Volume $Volume | ? { $_.initiator_group_name -eq $InitiatorGroup }
    }
    elseif($RemoveAll.IsPresent)
    {
        $NimbleACL = Get-NimbleVolumeACL -Volume $Volume
    }
    else
    {
        return
    }

    $header = @{ "X-Auth-Token" = $script:token }
    
    foreach($acl in $NimbleACL) {
        $uri = "https://" + $script:array + ":5392/v1/access_control_records/" + $acl.id
        $response = Invoke-RestMethod -Uri $uri -Method Delete -Headers $header
    }
}

Function Get-NimblePerformancePolicy
{
    param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $PerformancePolicy
    )

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/performance_policies/detail"
    if ($PerformancePolicy -ne $null)
    {
        $uri = $uri + "?name=" + $PerformancePolicy
    }

    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $header

    return $response.data
}

Function New-NimblePerformancePolicy
{
	param
	(
		[Parameter(Mandatory=$true)][string]$PolicyName,
		[Parameter(Mandatory=$false)][string]$BlockSize = "4096",
		[Parameter(Mandatory=$false)][string]$Cache = "true",
        [Parameter(Mandatory=$false)][string]$Compress = "true",
        [Parameter(Mandatory=$false)][string]$CachePolicy = "normal",
        [Parameter(Mandatory=$false)][string]$SpacePolicy = "offline"
	)  

	$data = @{
		name = $PolicyName
		block_size = $BlockSize = 4096
		cache = $Cache
        compress = $Compress
        cache_policy = $CachePolicy
        space_policy = $SpacePolicy
	}

	$body = convertto-json (@{ data = $data })
	$header = @{ "X-Auth-Token" = $script:token }
	$uri = "https://" + $script:array + ":5392/v1/performance_policies"
	$result = Invoke-RestMethod -Uri $uri -Method Post -Body $body -Header $header

}

Function Get-InitiatorGroup
{
    param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $InitiatorGroup
    )

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/initiator_groups/detail"
    if ($Volume -ne $null)
    {
        $uri = $uri + "?name=" + $InitiatorGroup
    }

    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $header

    return $response.data
}

Function Get-NimbleVolumeCollection
{
    param
    (
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $VolumeCollection
    )

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/volume_collections/detail"
    if ($VolumeCollection -ne $null)
    {
        $uri = $uri + "?name=" + $VolumeCollection
    }

    $response = Invoke-RestMethod -Uri $uri -Method Get -Headers $header

    return $response.data
}

Function Set-NimbleVolume
{
    param
    (
        [Parameter(Mandatory=$true)][string]$Volume,
        [Parameter(Mandatory=$false)][string]$VolumeCollection,
        [switch]$StopProtecting
    )

    $data = @{
	}

    $VolObject = Get-NimbleVolume -Volume $Volume
    if ($VolumeCollection -ne "") 
    {
        $VolCollObject = Get-NimbleVolumeCollection -VolumeCollection $VolumeCollection
        $data.Add("volcoll_id", $VolCollObject.id)
    }
    elseif ($StopProtecting.IsPresent)
    {
        $data.Add("volcoll_id", "")
    }
        
	$body = convertto-json (@{ data = $data })

    $header = @{ "X-Auth-Token" = $script:token }
    $uri = "https://" + $script:array + ":5392/v1/volumes/" + $VolObject.id

    $response = Invoke-RestMethod -Uri $uri -Method Put -Body $body -Headers $header
}


# Add PowerCLI SnapIn
if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null) {
  try {
    Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
  }
  catch {
    Write-Host "Please install VMware PowerCLI"
    Return
  }
}

# Connect to vCenter and get list of hosts
try {
  Connect-VIServer -Server $vcms_ip
  $vmhosts = Get-VMHost -ErrorAction Stop
}
catch {
  Write-Host "Could not connect to vCenter (" $vcms_ip ") and get hosts" $?
  Return
}

#Enumerate Worker Nodes
$vdbWorkers = Get-VM | ? { $_.name -like 'vdb-*' -and $_.name -ne 'vdb-command' }



#### Phase 1 Tests
#Setup 4K regular volumes
Reset-Workers -RemoveHardDisks -vdbWorkers $vdbWorkers
Clean-NimbleVolumes
<#
for ($i = 1; $i -le 16; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-4ktest-$i" -VolumeSizeMB 102400 -PerformancePolicy "P4K"
    New-NimbleVolumeACL -Volume "vdbench-4ktest-$i" -InitiatorGroup $initiatorGroup 
}

Reset-Workers -AddHardDisks -StartVM -vdbWorkers $vdbWorkers
Start-Sleep -Seconds 120
Write-Host "Start 4k phase 1 workload"
Write-Host "./afc_run -o TEST/4KRUN1 -t 4k100r -p 10m -c 20m -s 20m -w 4"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KRUN1 -t 4k100r -p 10m -c 20m -s 20m -w 4"
Write-Host "./afc_run -o TEST/4KRUN2 -t 4k50r50w -p 10m -c 20m -s 20m -w 4 -R 50"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KRUN2 -t 4k50r50w -p 10m -c 20m -s 20m -w 4 -R 50"
Write-Host "./afc_run -o TEST/4KRUN3 -t 4k100w -p 10m -c 20m -s 20m -w 4 -R 0"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KRUN3 -t 4k100w -p 10m -c 20m -s 20m -w 4 -R 0"

#### Phase 2 Tests
#Setup 256K regular volumes
Reset-Workers -RemoveHardDisks -vdbWorkers $vdbWorkers
Clean-NimbleVolumes

for ($i = 1; $i -le 16; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-256ktest-$i" -VolumeSizeMB 102400 -PerformancePolicy "P32K"
    New-NimbleVolumeACL -Volume "vdbench-256ktest-$i" -InitiatorGroup $initiatorGroup 
}

Reset-Workers -AddHardDisks -StartVM -vdbWorkers $vdbWorkers
Start-Sleep -Seconds 120
Write-Host "Start 256k phase 2 workload"
Write-Host "./afc_run -o TEST/256RUN1 -t 256K100r -p 10m -c 20m -s 20m -w 256 -l s"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/256RUN1 -t 256K100r -p 10m -c 20m -s 20m -w 256 -l s"
Write-Host "./afc_run -o TEST/256RUN2 -t 256K100w -p 10m -c 20m -s 20m -w 256 -R 0 -l s"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/256RUN2 -t 256K100w -p 10m -c 20m -s 20m -w 256 -R 0 -l s"



#### Phase 3 Tests

#Setup 4K full array volumes
Reset-Workers -RemoveHardDisks -vdbWorkers $vdbWorkers
Clean-NimbleVolumes


for ($i = 1; $i -le 8; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-4ktest-$i" -VolumeSizeMB $volumeSize[$afa_model] -PerformancePolicy "P4K"
    New-NimbleVolumeACL -Volume "vdbench-4ktest-$i" -InitiatorGroup $initiatorGroup 
}

Reset-Workers -AddHardDisks -vdbWorkers $vdbWorkers

for ($i = 1; $i -le 8; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-256ktest-$i" -VolumeSizeMB $volumeSize[$afa_model] -PerformancePolicy "P32K"
    New-NimbleVolumeACL -Volume "vdbench-256ktest-$i" -InitiatorGroup $initiatorGroup 
}

Reset-Workers -AddHardDisks -StartVM -vdbWorkers $vdbWorkers

Start-Sleep -Seconds 120
Write-Host "Start 4k full array phase 3 workload"
if($afa_model -eq "AF-7000")
{
    Write-Host "./afc_run -o TEST/FILL1 -t fill1 -P 10T -c 10m -s 10m -w 256 -l sequential"
    & $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/FILL1 -t fill1 -P 10T -c 10m -s 10m -w 256 -l sequential"
}
else
{
    Write-Host "./afc_run -o TEST/FILL1 -t fill1 -P 20T -c 10m -s 10m -w 256 -l sequential"
    & $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/FILL1 -t fill1 -P 20T -c 10m -s 10m -w 256 -l sequential"
}

Write-Host "./afc_run -o TEST/4KSustained -t 4k50r50w -p 2h -c 2h -s 2h -w 4 -R 50"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KSustained -t 4k50r50w -p 2h -c 2h -s 2h -w 4 -R 50"


Write-Host "Start 256k full array phase 3 workload"
Write-Host "./afc_run -o TEST/256RUN3 -t 256K50r50w -p 2h -c 2h -s 2h -w 256 -R 50 -l s"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/256RUN3 -t 256K50r50w -p 2h -c 2h -s 2h -w 256 -R 50 -l s"


#### Phase 4 Tests

#Setup 4K snapshot array volumes
Reset-Workers -RemoveHardDisks -vdbWorkers $vdbWorkers
Clean-NimbleVolumes

for ($i = 1; $i -le 16; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-4ktest-$i" -VolumeSizeMB $volumeSize[$afa_model] -PerformancePolicy "P4K"
    New-NimbleVolumeACL -Volume "vdbench-4ktest-$i" -InitiatorGroup $initiatorGroup 
    Set-NimbleVolume -Volume "vdbench-4ktest-$i" -VolumeCollection "vdbench"
}

Reset-Workers -AddHardDisks -StartVM -vdbWorkers $vdbWorkers
Start-Sleep -Seconds 120
Write-Host "Start 4k Phase 4 snapshot workload"
Write-Host "./afc_run -o TEST/4KRUNSNAP -t 4k100wSnap -p 10m -c 20m -s 8h -w 4 -R 0"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KRUNSNAP -t 4k100wSnap -p 10m -c 20m -s 8h -w 4 -R 0"


#Setup 4K snapshot array volumes
Reset-Workers -RemoveHardDisks -vdbWorkers $vdbWorkers
Clean-NimbleVolumes

for ($i = 1; $i -le 16; $i++)
{
    New-NimbleVolume -VolumeName "vdbench-4ktest-$i" -VolumeSizeMB $volumeSize[$afa_model] -PerformancePolicy "P4K" -Encrypt
    New-NimbleVolumeACL -Volume "vdbench-4ktest-$i" -InitiatorGroup $initiatorGroup 
}

Reset-Workers -AddHardDisks -StartVM -vdbWorkers $vdbWorkers
Start-Sleep -Seconds 120
Write-Host "Start 4k Phase 4 encrypt workload"
Write-Host "./afc_run -o TEST/4KRUNENCRYPT -t 4k100encrypt -P 1T -c 30m -s 4h -w 4"
& $plink -l "root" -pw "Nimblese123" $vdb_command_host "./afc_run -o TEST/4KRUNENCRYPT -t 4k100encrypt -P 1T -c 30m -s 4h -w 4"

<##>