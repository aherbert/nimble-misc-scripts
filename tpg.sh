#!/bin/bash

#1)      Create 20 LUNs (2 for each systems, i.e. 10 systems total)
#2)      Establish replication relationship for these 10 systems (20 LUNs)
#3)      Initiate failover for these 10 systems
#4)      Reverse replicate these 10 systems
#5)      Initiate failback
#6)      Grow LUN size for one of the systems
#7)      Initiate failover/failback for that particular system

vol --create boot-vol-01 --size 10240 --initiatorgrp host-01
# After first OS install
vol --snap boot-vol-01 --snapname boot-clone
for x in `seq 2..27`; do vol --clone boot-vol-01 --snapname -boot-clone --clonename boot-vol-$(printf "%02d" $x) --initiatorgrp host-$(printf "%02d" $x); done

PROD_ARRAY="10.64.13.200"
DR_ARRAY="10.64.13.200"
VOLCOLL="AH-VolCol"
SUFFIX="Backup"
SSHKEY="/Users/aherbert/.ssh/nimble"
SSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null admin@${PROD_ARRAY} -i ${SSHKEY}"
DRSSHCMD="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null admin@${DR_ARRAY} -i ${SSHKEY}"

VOLS=$(${SSHCMD} "volcoll --info ${VOLCOLL}" | grep "Associated volumes" | cut -c 21- | tr -d ,)

# Assuming the hosts use igroups of igroup-host-01 - igroup-host-10
# Create the 20 volumes, 2 per host
for host in `seq -f "%02g" 1..10`
do
  ${SSHCMD} "vol --create vol-host-${host}-vol-01 --size 102400 --initiatorgrp igroup-host-${host}"
  ${SSHCMD} "vol --create vol-host-${host}-vol-02 --size 102400 --initiatorgrp igroup-host-${host}"
done

# Create the volcoll for replication. For this sample 1 volcoll per host so per host failover is possible.
for host in `seq -f "%02g" 1..10`
do
  ${SSHCMD} "volcoll --create volcoll-host-${host}"
  ${SSHCMD} "volcoll --addsched volcoll-host-${host} --schedule 5min --repeat 5 --repeat_unit minutes --retain 48 --replicate_to DR-Array --num_retain_replica 48"
  ${SSHCMD} "volcoll --addsched volcoll-host-${host} --schedule 1hour --repeat 1 --repeat_unit hours --retain 48 --replicate_to DR-Array --num_retain_replica 48"
  ${SSHCMD} "vol --assoc vol-host-${host}-vol-01 --volcoll volcoll-host-${host}"
  ${SSHCMD} "vol --assoc vol-host-${host}-vol-02 --volcoll volcoll-host-${host}"
done


# Failover
for host in `seq -f "%02g" 1..10`
do
  ${DRSSHCMD} "volcoll --promote volcoll-host-${host}"
done

# Reverse Replication
for host in `seq -f "%02g" 1..10`
do
  ${DRSSHCMD} "volcoll --promote volcoll-host-${host}"
done

# Reverse Replication
for host in `seq -f "%02g" 1..10`
do
  ${SSHCMD} "volcoll --demote volcoll-host-${host} --partner DR-Array"
  ${DRSSHCMD} "volcoll --editsched 5min --replicate_to Prod-Array --num_retain_replica 48"
  ${DRSSHCMD} "volcoll --editsched 1hour --replicate_to Prod-Array --num_retain_replica 48"
done

# Showing failback using handover since it is planned. Replication reversal is automatic
for host in `seq -f "%02g" 1..10`
do
  ${DRSSHCMD} "volcoll --handover volcoll-host-${host}"
done

${SSHCMD} "vol --edit vol-host-01-vol-01 --size 204800"





