#!/bin/sh

set -e

# Some basic checking on the validity of $1
if [ -z "$1" ] ; then
        echo "Usage $0 /dev/nimblestoragedevice" 1>&2
        exit 1
fi

if [ ! -b "$1" ] ; then
        echo "$0: $1 is not a special file" 1>&2
        exit 1
fi

SG_INQ=$(which sg_inq)
if [ ! -e $SG_INQ ] ; then
        echo "$0: sg3_utils is required for this script" 1>&2
        exit 1
fi

VOL=$($SG_INQ -p0xd4 -r "$1" | tr -d '\200-\377')
if [ ! -z "$VOL" ] ; then
        echo $VOL
fi