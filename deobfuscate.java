View Javadoc

1   package org.apache.tomcat.maven.runner;
2   
3   /*
4    * Licensed to the Apache Software Foundation (ASF) under one
5    * or more contributor license agreements.  See the NOTICE file
6    * distributed with this work for additional information
7    * regarding copyright ownership.  The ASF licenses this file
8    * to you under the Apache License, Version 2.0 (the
9    * "License"); you may not use this file except in compliance
10   * with the License.  You may obtain a copy of the License at
11   *
12   *   http://www.apache.org/licenses/LICENSE-2.0
13   *
14   * Unless required by applicable law or agreed to in writing,
15   * software distributed under the License is distributed on an
16   * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
17   * KIND, either express or implied.  See the License for the
18   * specific language governing permissions and limitations
19   * under the License.
20   */
21  
22  import java.util.Properties;
23  
24  
25  /**
26   * Password obfuscate utility class. Lifted from Jetty org.mortbay.jetty.security.Password
27   * <p/>
28   * <p/>
29   * Passwords that begin with OBF: are de obfuscated.
30   * <p/>
31   * Passwords can be obfuscated by running Obfuscate as a main class. Obfuscated password are required if a system needs
32   * to recover the full password (eg. so that it may be passed to another system).
33   * <p/>
34   * They are not secure, but prevent casual observation.
35   *
36   * @see <a
37   *      href="http://grepcode.com/file_/repo1.maven.org/maven2/org.mortbay.jetty/jetty/6.1.11/org/mortbay/jetty/security/Password.java/?v=source"
38   *      >Jetty Source org.mortbay.jetty.security.Password</a>
39   * @since 2.0
40   */
41  public class PasswordUtil
42  {
43      public static final String __OBFUSCATE = "OBF:";
44  
45      /* ------------------------------------------------------------ */
46      public static String obfuscate( String s )
47      {
48          StringBuilder buf = new StringBuilder();
49          byte[] b = s.getBytes();
50  
51          buf.append( __OBFUSCATE );
52          for ( int i = 0; i < b.length; i++ )
53          {
54              byte b1 = b[i];
55              byte b2 = b[s.length() - ( i + 1 )];
56              int i1 = 127 + b1 + b2;
57              int i2 = 127 + b1 - b2;
58              int i0 = i1 * 256 + i2;
59              String x = Integer.toString( i0, 36 );
60  
61              switch ( x.length() )
62              {
63                  case 1:
64                      buf.append( '0' );
65                  case 2:
66                      buf.append( '0' );
67                  case 3:
68                      buf.append( '0' );
69                  default:
70                      buf.append( x );
71              }
72          }
73          return buf.toString();
74  
75      }
76  
77      /* ------------------------------------------------------------ */
78      public static String deobfuscate( String s )
79      {
80          if ( s.startsWith( __OBFUSCATE ) )
81          {
82              s = s.substring( __OBFUSCATE.length() );
83  
84              byte[] b = new byte[s.length() / 2];
85              int l = 0;
86              for ( int i = 0; i < s.length(); i += 4 )
87              {
88                  String x = s.substring( i, i + 4 );
89                  int i0 = Integer.parseInt( x, 36 );
90                  int i1 = ( i0 / 256 );
91                  int i2 = ( i0 % 256 );
92                  b[l++] = (byte) ( ( i1 + i2 - 254 ) / 2 );
93              }
94              return new String( b, 0, l );
95          }
96          else
97          {
98              return s;
99          }
100 
101     }
102 
103     public static void deobfuscateSystemProps()
104     {
105         Properties props = System.getProperties();
106         for ( Object obj : props.keySet() )
107         {
108             if ( obj instanceof String )
109             {
110                 String key = (String) obj;
111                 String value = (String) props.getProperty( key );
112                 if ( value != null && value.startsWith( __OBFUSCATE ) )
113                 {
114                     System.setProperty( key, deobfuscate( value ) );
115                 }
116             }
117         }
118     }
119 
120     public static void main( String[] args )
121     {
122         if ( args[0].startsWith( __OBFUSCATE ) )
123         {
124             System.out.println( PasswordUtil.deobfuscate( args[1] ) );
125         }
126         else
127         {
128             System.out.println( PasswordUtil.obfuscate( args[1] ) );
129         }
130     }
131 }
