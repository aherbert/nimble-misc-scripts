﻿$VCServer = "10.0.0.10"
$VCUser = "Administrator"
$VCPassword = "Password"
$SSHUser = "root"
$SSHPassword = "Password"

if ((Get-PSSnapin -Name VMware.VimAutomation.Core -ErrorAction SilentlyContinue) -eq $null) {
  try {
    Add-PSSnapin VMware.VimAutomation.Core -ErrorAction Stop
  }
  catch {
    Write-Host "Please install VMware PowerCLI"
    Return
  }
}

if ($VCUser -ne "") {
  $vc = Connect-VIServer -Server $VCServer -WarningAction 0 -User $VCUser -Password $VCPassword
}
else{
  $vc = Connect-VIServer -Server $VCServer -WarningAction 0
}

$ds_list = Get-Datastore

foreach ($ds in $ds_list) {
  $vmhost = Get-VMHost -Datastore $ds | Select -Last 1
  Write-Host "Connected to ESXi host" $vmhost.Name
  $esxcli = Get-EsxCli -VMHost $vmhost

  $lun = Get-ScsiLun -Datastore $ds
  if ($lun.Vendor -eq "Nimble") {
    Write-Host "Reclaiming space on Nimble volume" $ds.Name
    if ($vmhost.Version -match "^5.0") {
      Write-Output "y" | c:\plink.exe -l $SSHUser -pw $SSHPassword $vmhost.Name "cd /vmfs/volumes/$($ds.Name) && vmkfstools -y 80" 
    }
    else {
      $esxcli.storage.vmfs.unmap("60000", $ds.Name, $null)
    }
  }
  else {
    Write-Host $ds.Name "is not a Nimble volume, skipping."
  }
}