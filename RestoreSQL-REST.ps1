### CONFIG VARIABLES ###
$ArrayIP = "172.31.223.6"
$ArrayUsername = "admin"
$ArrayPassword = "admin"
$VolumeCollection = "SQLServer"
$InitiatorGroup = "SQLServerQA"
$SQLInstance = "MSSQLSERVER"

# Load Nimble Module
Import-Module NimblePowershellToolKit

# Refresh iSCSI Portals
Function Update-IscsiTargetList {
  $(Get-WmiObject -Namespace root/wmi MSiSCSIInitiator_MethodClass).RefreshTargetList() | Out-Null
}

# Get list of established sessions and their respective IPs and ports
Function Get-IscsiEstablishedSessions {
  $SessionClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_SessionClass
  foreach ($Session in $SessionClass) {
    if ($Session -eq $null) {
      continue;
    }
    $SessionConnectionInformation = $Session.GetPropertyValue("ConnectionInformation").Get(0)
    New-Object PSObject -Property @{
	    Target = $Session.TargetName
	    TargetNice = $Session.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
      TargetAddress = $SessionConnectionInformation.TargetAddress
      TargetPort = $SessionConnectionInformation.TargetPort
	    InitiatorAddress = $SessionConnectionInformation.InitiatorAddress
      InitiatorPort = $SessionConnectionInformation.InitiatorPort
	    Devices = $Session.Devices
      Session = $Session
    }
  }
}

# Get list of Persistent Logins
Function Get-IscsiPersistentLogins {
  $PersistentLoginClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_PersistentLoginClass
  foreach ($PersistentLogin in $PersistentLoginClass) {
    if ($PersistentLogin -eq $null) {
      continue;
    }
	  New-Object PSObject -Property @{
	    Target = $PersistentLogin.TargetName
	    TargetNice = $PersistentLogin.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"	  
      Initiator = $PersistentLogin.InitiatorInstance
      InitiatorPort = $PersistentLogin.InitiatorPortNumber
      TargetAddress = $PersistentLogin.TargetPortal.Address
	    TargetPort = $PersistentLogin.TargetPortal.Port
	  }
  }
}

# Use password above for PSCredential Object
$securePassword = ConvertTo-SecureString -String $ArrayPassword -AsPlainText -Force
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $ArrayUsername, $securePassword

# Connect to Nimble array
Connect-NSGroup -group $ArrayIP -credential $credential

# Shutdown SQL Services
$services = Get-Service | ? {$_.DisplayName -imatch "SQL Server.*$SQLInstance"}
foreach($service in $services) {
  Write-Host "Stopping SQL service " $service.Name
  Stop-Service -Force -Name $service.Name
}

# Get Snapshot list for Volume Collection
$snapColl = Get-NSSnapshotCollection -sortBy name | ? { $_.volcoll_name -eq $VolumeCollection } | Select-Object -Last 1
Write-Host "Using most recent snapshot "$snapColl.name" with the following volumes:"
$snapColl.snapshots_list | Format-Table vol_name

# Loop through each volume
$cloneObjects = @()
foreach($snap in $snapColl.snapshots_list) {

  # Set Volume Variables
  $cloneName = $snap.vol_name + "-" + $env:COMPUTERNAME + "-" + $SQLInstance
  $cloneDescription = "Auto clone for " + $snap.vol_name + " to " + $env:COMPUTERNAME + ":" + $SQLInstance

  # Disconnect from volume
  $cloneSessions = Get-IscsiSession | ? { $_.TargetNodeAddress -ilike "*nimblestorage:$($cloneName)-*" }
  $cloneSessions | ? { $_.IsPersistent -eq $true } | Unregister-IscsiSession
  $cloneSessions | % { Disconnect-IscsiTarget -NodeAddress $_.TargetNodeAddress -Confirm:$false }

  # Remove old clone if still exists
  $oldClone = Get-NSVolume -name $cloneName | ? { $_.clone -eq $true }
  if ($oldClone -ne $null) 
  {
    Set-NSVolume -id $oldClone.id -online $false -force $true
    Write-Host "Set old volume " $oldClone.name " offline"
    Remove-NSVolume -id $oldClone.id
    Write-Host "Deleted old volume " $oldClone.name
  }

  $cloneObject = New-NSClone -name $cloneName -description $cloneDescription -base_snap_id $snap.id -clone $true -online $true
  $cloneObjects += $cloneObject

  # Set Access Control on new clone
  foreach ($acl in $cloneObject.access_control_records)
  {
    Remove-NSAccessControlRecord -id $acl.acl_id
  }
  $igroupObject = Get-NSInitiatorGroup -name $InitiatorGroup
  $result = New-NSAccessControlRecord -apply_to "both" -initiator_group_id $igroupObject.id -vol_id $cloneObject.id
}

Write-Host "Refreshing to find new targets"
Update-IscsiTargetList | Out-Null

# Connect to volume clones
foreach ($cloneObject in $cloneObjects) { 
  Connect-IscsiTarget -NodeAddress $cloneObject.target_name -IsPersistent $true -IsMultipathEnabled $true | Out-Null
}

# Wait 5 seconds for connections to finalize
Write-Host "Rescanning for new drives"
"rescan `n exit" | diskpart | out-null
Start-Sleep -Seconds 10

# Get current set of Nimble sessions
$NimbleDisks = Get-EstablishedSessions | ? {$_.Target -match "com.nimblestorage"} | ? {$_.Target -match $env:COMPUTERNAME + "-" + $SQLInstance } | Select Target,TargetNice,Devices -Unique

# Loop Nimble Disks and make sure they are online and mount if not already mounted
foreach ($NimbleDisk in $NimbleDisks) {
  Write-Host "Setting " $NimbleDisk.TargetNice " online"
  "select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n attributes disk clear readonly noerr`n online `n online disk" | diskpart | out-null
  $Disk = Get-WmiObject -Class Win32_DiskDrive | ? {$_.Caption -match "Nimble Server" -and $_.DeviceID -eq $NimbleDisk.Devices[0].LegacyName}
  $Partitions = Get-WmiObject -Query "ASSOCIATORS OF {$($Disk.Path.RelativePath)}" | ? {$_.__CLASS -eq "Win32_DiskPartition"}
  foreach ($Partition in $Partitions) {
	  $Path = "c:\nimble\"+$NimbleDisk.TargetNice+"\"+$Partition.Index
	  if(Test-Path -Path $Path -PathType Container) {
	    mountvol $Path /d
	  }
	  else {
	    New-Item $Path -ItemType Directory | Out-Null
	  }
	  Write-Host "Mounting " $NimbleDisk.TargetNice " @ " $Path
	  "select disk "+$NimbleDisk.Devices[0].DeviceNumber+"`n select partition "+$($Partition.Index + 1)+"`n attributes volume clear readonly noerr`n attributes volume clear hidden noerr`n attributes volume clear shadowcopy noerr`n assign mount="+$Path | diskpart | out-null
	  chkdsk /x $Path | out-null
  }
}

# Restart SQL Services
foreach($service in $services) {
  if((Get-WmiObject Win32_Service -filter ("Name='"+$service.Name+"'")).StartMode -eq "Auto") {
    Write-Host "Starting SQL service " $service.Name
    Start-Service -Name $service.Name
  }
}

Write-Host "Finished!"