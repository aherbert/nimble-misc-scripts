#!/bin/bash

ARRAY="172.31.223.6"
VOL="MySQL"
CLONE="$VOL-clone"
INITGRP="MySQL"
MOUNT="/mnt/mysql-clone"
FSTYPE="ext4"
SSH="ssh -i $HOME/.ssh/nimble -o StrictHostKeyChecking=no admin@$ARRAY"

#Get Latest Snap Name
SNAP=$($SSH snap --list --vol $VOL | tail -n +5 | head -n1 | tr -s ' ' | cut -d" " -f 2); 

#Unmount old clone
if [ "$(mount | grep -c $MOUNT)" = "1" ]; then { 
  umount $MOUNT || { echo "Can't unmount $MOUNT!!"; exit 1; } 
}; fi

#Disconnect iSCSI session
iscsiadm -m node -u -T $(iscsiadm -m session | egrep -i ":$CLONE-[^-]*$" | cut -d" " -f 4 | uniq)

#Remove old clone
$SSH vol --offline $VOL-clone  --force
$SSH vol --delete $VOL-clone 

#Create new clone
$SSH vol --clone $VOL --snapname $SNAP --clonename $CLONE --apply_acl_to both --readonly no --initiatorgrp $INITGRP

#Scan for new target and connect
iscsiadm -m discovery | cut -d" " -f 1 | xargs -L1 iscsiadm -m discovery -t sendtargets -p
iscsiadm -m node -l -T $(iscsiadm -m node | egrep -i ":$CLONE-[^-]*$" | cut -d" " -f 2 | uniq)
sleep 5
multipath -F
multipath -r

#Mount volume
DEVICE=/dev/mapper/$(ls /dev/mapper/ | grep -i "^$CLONE.*-part1")
if [ "$(grep -c $MOUNT /etc/fstab)" = "1" ]; then {
  sed -i -e "s#.*$MOUNT.*#$DEVICE $MOUNT $FSTYPE _netdev 0 0#" /etc/fstab
}; else {
  cat <<EOF >>/etc/fstab
$DEVICE $MOUNT $FSTYPE _netdev 0 0
EOF
}; fi
mount $MOUNT
