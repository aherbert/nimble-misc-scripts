## <copyright file="Delete-OldUnmanagedSnaps.ps1" company="NimbleStorage">
## Copyright (c) 2016 All Right Reserved, http://www.nimblestorage.com/
##
## THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
## KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
## IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
## PARTICULAR PURPOSE.
##
## </copyright>
## <author>Adam Herbert</author>
## <email>aherbert@nimblestorage.com</email>
## <date>2016-11-09</date>
## <summary>The workflow of the script is to search for all unmanaged snapshots 
## that are older than the number of snpashotDays days listed. Then it loops 
## through them and requests them to be deleted.</summary>

$nimbleGroupIP = "af7000.nimblese.lab"
$nimbleUsername = ""
$nimblePassword = ""
$snapshotDays = 14

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

function Main
{
  $nsArrayToken = Connect-NSArray -GroupAddress $nimbleGroupIP -UserName $nimbleUsername -Password $nimblePassword
  Write-Host "Logged into" $nimbleGroupIP

  $epochTime = (New-TimeSpan -Start (Get-Date -Date "01/01/1970") -End (Get-Date).ToUniversalTime()).TotalSeconds
  $oldestSnapshotDate = $epochTime - ($snapshotDays * 24 * 60 * 60)
  $response = Send-NSRequest -RequestMethod "GET" -RequestTarget "snapshots/detail" -RequestArguments @{"is_unmanaged" = "true"}
  $snaps = $response.data | ? {$_.is_unmanaged -eq $true -and $_.creation_time -lt $oldestSnapshotDate}

  foreach ($snap in $snaps)
  {
    try 
    {
      Write-Host "Deleting snapshot" $snap.name "from" $snap.vol_name
      $response = Send-NSRequest -RequestMethod "DELETE" -RequestTarget $("snapshots/" + $snap.id)
    }
    catch
    {
      Write-Warning "Couldn't delete snapshot" 
      Write-Warning $_.Exception.Message
    }
  }

  # Logout
  Write-Host "Logging Out"
  $response = Send-NSRequest -RequestMethod "DELETE" -RequestTarget $("tokens/" + $nsArrayToken.id)
  $response.StatusCode
}

function Connect-NSArray 
{
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $GroupAddress,

        [string]
        $UserName="admin",

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]
        $Password
    )

    $script:nsGroupAddress = $GroupAddress

    $data = @{
        'username' = $UserName
        'password' = $Password
    }

    $return = Send-NSRequest -RequestMethod 'post' -RequestTarget 'tokens' -RequestArguments $data

    $script:nsAuthToken = $return.data.session_token

    Return $return.data

}

function Send-NSRequest
{
<#
.SYNOPSIS
Abstract the Invoke-Restmethod for all other module cmdlets

.DESCRIPTION
.PARAMETER RequestMethod
  Set the request method used. Valid options are 'get', 'delete', 'post', 'put'
.PARAMETER RequestTarget
  Set the request object and any sub object
.PARAMETER RequestArguments
  Set URI arguments for GET requests
.PARAMETER RequestHeaders
  Set any additional headers. The X-Auth-Token header is set automatically from the $site:nsAuthToken variable
.PARAMETER RequestBody
  Set the Request Body. 
.INPUTS
.OUTPUTS
.EXAMPLE
.EXAMPLE
.LINK
#>
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [ValidateSet('get', 'delete', 'post', 'put')]
        [string]$RequestMethod,

        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [string]$RequestTarget,
        
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestArguments,

        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [hashtable]$RequestHeaders = @{}
    )

    if ( $script:nsAuthToken -ne $null ) { $RequestHeaders.Add("X-Auth-Token", $script:nsAuthToken) }

    $uri = 'https://' + $script:nsGroupAddress + ':5392/'
    if ($RequestTarget -eq 'versions')
    {
        $uri = $uri + 'versions'
    }
    else
    {
        $uri = $uri + 'v1/' + $RequestTarget
    }


    if ( $RequestMethod -ieq 'get' )
    {
        if ($RequestArguments.Count -gt 0)
        {
            $uri += '?'
            $uri += [string]::join("&", @(foreach($pair in $RequestArguments.GetEnumerator()) { if ($pair.Name) { $pair.Name + '=' + $pair.Value } }))
        }
    }
    else
    {
        $RequestJSON = ConvertTo-Json(@{ data = $RequestArguments })
    }

    try
    {
        $request = [System.Net.WebRequest]::Create($uri)
        $request.Method = $RequestMethod.ToUpper()
        $request.ContentType = "application/json"
        $RequestHeaders.GetEnumerator() | % { $request.Headers.Add($_.Key, $_.Value) }
        if ( $RequestMethod -ine 'GET' -and $RequestArguments.Count -gt 0 )
        {
            $requestStream = $request.GetRequestStream();
            $streamWriter = New-Object System.IO.StreamWriter $requestStream
            $streamWriter.Write($RequestJSON.ToString())
            $streamWriter.Flush()
            $streamWriter.Close()
        }
 
        $response = $request.GetResponse()
    }
    catch
    {
        $httpStatus = [regex]::matches($_.exception.message, "(?<=\()[\d]{3}").Value
        if ( $httpStatus -eq '401' )
        {
            Write-Warning 'You must login into the Nimble Array with Connect-NSArray'
            Exit
        }
        elseif ($httpStatus -ne $null)
        {
            $responseStream = $_.Exception.InnerException.Response.GetResponseStream()
            $readStream = New-Object System.IO.StreamReader $responseStream
            $data = $readStream.ReadToEnd()
            $results = $data | ConvertFrom-Json
            Throw "$('There was an error, status code: ' + $httpStatus) `n $($results.messages | Format-Table | Out-String)"
        }
        else
        { 
            Throw $_.Exception
        }
        Return
    }
    finally
    {
        #if ($null -ne $streamWriter) { $streamWriter.Dispose() }
        #if ($null -ne $requestStream) { $requestStream.Dispose() }
    }
    $responseStream = $response.GetResponseStream()
    $readStream = New-Object System.IO.StreamReader $responseStream
    $data = $readStream.ReadToEnd()
    #$response.Close()
    $results = New-Object -TypeName PSCustomObject
    $results | Add-Member -MemberType NoteProperty -Name StatusCode -Value "$([int]$response.StatusCode) - $($response.StatusCode)" 
    $results | Add-Member -MemberType NoteProperty -Name data -Value $($data | ConvertFrom-Json).data

    Return $results
}

Main