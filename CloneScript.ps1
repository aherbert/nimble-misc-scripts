$nimbleGroupIP = "cs500.nimblese.lab"
$nimbleUsername = "sedemo1"
$nimblePassword = "Nimblese123"
$volumeName = "aherbert-win2012"
$cloneName = "aherbert-win2012-clone"
$cloneInitiatorGroup = "aherbert-win2012"

Import-Module HPENimblePowershellToolKit

# Get list of established sessions and their respective IPs and ports
Function Get-IscsiEstablishedSessions {
  $SessionClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_SessionClass
  foreach ($Session in $SessionClass) {
    if ($Session -eq $null) {
      continue;
    }
    $SessionConnectionInformation = $Session.GetPropertyValue("ConnectionInformation").Get(0)
    New-Object PSObject -Property @{
      Target           = $Session.TargetName
      TargetNice       = $Session.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"
      TargetAddress    = $SessionConnectionInformation.TargetAddress
      TargetPort       = $SessionConnectionInformation.TargetPort
      InitiatorAddress = $SessionConnectionInformation.InitiatorAddress
      InitiatorPort    = $SessionConnectionInformation.InitiatorPort
      Devices          = $Session.Devices
      Session          = $Session
    }
  }
}

# Get list of Persistent Logins
Function Get-IscsiPersistentLogins {
  $PersistentLoginClass = Get-WmiObject -Namespace root\wmi MSiSCSIInitiator_PersistentLoginClass
  foreach ($PersistentLogin in $PersistentLoginClass) {
    if ($PersistentLogin -eq $null) {
      continue;
    }
    New-Object PSObject -Property @{
      Target        = $PersistentLogin.TargetName
      TargetNice    = $PersistentLogin.TargetName -replace "^[^:]+:(.+)-\w+\.\w+\.\w+", "`$1"	  
      Initiator     = $PersistentLogin.InitiatorInstance
      InitiatorPort = $PersistentLogin.InitiatorPortNumber
      TargetAddress = $PersistentLogin.TargetPortal.Address
      TargetPort    = $PersistentLogin.TargetPortal.Port
    }
  }
}

# Use password above for PSCredential Object
$securePassword = ConvertTo-SecureString -String $nimblePassword -AsPlainText -Force
$credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $nimbleUsername, $securePassword
#$credential = Get-Credential

$nimble = Connect-NSGroup -group $nimbleGroupIP -credential $credential
Write-Host ("Connected to Nimble group " + $nimble.Group)

# Get parent volume object
$volObject = Get-NSVolume -name $volumeName

# Disconnect from volume
$cloneSessions = Get-IscsiEstablishedSessions | Where-Object { $_.TargetNice -ieq $cloneName } 
$cloneSessions | ForEach-Object { $_.Session.Logout() | Out-Null }
$clonePersistentLogins = Get-IscsiPersistentLogins | Where-Object { $_.TargetNice -ieq $cloneName }
$clonePersistentLogins | ForEach-Object { iscsicli removepersistenttarget $_.Initiator $_.Target $_.InitiatorPort $_.TargetAddress $_.TargetPort | Out-Null }

# Delete Old Clone and Snapshot (if they exist)
$cloneObject = Get-NSVolume -name $cloneName
if ($cloneObject -ne $null) {
  Write-Host ("Removing old clone " + $cloneObject.name)
  Set-NSVolume -id $cloneObject.id -online $false -force $true | Out-Null
  Remove-NSVolume -id $cloneObject.id | Out-Null
}
$snapObject = Get-NSSnapshot -name $cloneName -vol_id $volObject.id
if ($snapObject -ne $null) {
  Write-Host ("Removing old snapshot " + $snapObject.name)
  Remove-NSSnapshot -id $snapObject.id | Out-Null
}

#Create new snapshot
#$snapObject = New-NSSnapshot -name $cloneName -vol_id $volObject.id

# Use Midnight Snapshot
$snapObject = Get-NSSnapshot -vol_id $volObject.id | Where-Object { $_.name -match '::00:00' } | Sort-Object -Property name | Select-Object -Last 1

Write-Host ("Creating new clone from snapshot " + $snapObject.name)

#Create clone from snapshot
$cloneObject = New-NSVolume -name $cloneName -base_snap_id $snapObject.id -clone $true -online $true
Write-Host ("Clone created " + $cloneObject.name)

# Set Access Control on new clone
$cloneObject.access_control_records | ForEach-Object { Remove-NSAccessControlRecord -id $_.id }
$igroupObject = Get-NSInitiatorGroup -name $cloneInitiatorGroup
New-NSAccessControlRecord -apply_to "both" -initiator_group_id $igroupObject.id -vol_id $cloneObject.id | Out-Null
Write-Host "ACL applied to new clone"

# Connect to volume
Update-IscsiTarget | Out-Null
$cloneTarget = Get-IscsiTarget | Where-Object { $_.NodeAddress -ilike "*nimblestorage:$($cloneName)-*" }
Connect-IscsiTarget -NodeAddress $cloneTarget.NodeAddress -IsPersistent $true -IsMultipathEnabled $true | Out-Null
Write-Host ("Connected to clone " + $cloneTarget.NodeAddress)